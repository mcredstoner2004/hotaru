from copy import deepcopy
import json


class Permissions:
    def __init__(self, ctx, cog: str = None, command: str = None):
        self.ctx = ctx
        self.cog = cog
        self.command = command
        self.permissions = self.ctx.bot.permissions
        self.perms = {}

    def init(self):
        self.permissions[str(self.ctx.guild.id)] = {}
        for name in self.ctx.bot.cogs.keys():
            self.permissions[str(self.ctx.guild.id)][name.lower()] = {
                "commands": {},
                "permissions": {},
            }
            self.permissions[str(self.ctx.guild.id)][name.lower()][
                "permissions"
            ] = deepcopy(self.perms)
        self.write()
        return self

    def real(self):
        try:
            cogs = [name.lower() for name in self.ctx.bot.cogs.keys()]
            commands = [
                command.name
                for command in self.ctx.bot.get_cog(self.cog.title()).get_commands()
            ]
            if self.command is not None:
                if self.command in commands:
                    return True
                else:
                    return False
            else:
                if self.cog in cogs:
                    return True
                else:
                    return False
        except:  # TODO make except more specific
            return False

    def load(self):
        if self.cog not in self.permissions[str(self.ctx.guild.id)]:
            print("Cog not found, initializing.")
            self.clear()
            return self
        if self.cog is not None and self.command is None:
            self.perms = self.permissions[str(self.ctx.guild.id)][self.cog][
                "permissions"
            ]
        elif self.cog is not None and self.command is not None:
            if (
                    self.command
                    not in self.permissions[str(self.ctx.guild.id)][self.cog]["commands"]
            ):
                print("Command not found, initializing.")
                self.clear()
                return self
            self.perms = self.permissions[str(self.ctx.guild.id)][self.cog]["commands"][
                self.command
            ]["permissions"]
        else:
            print("Cog argument is missing.")
        return self

    def save(self, write=True):
        if self.cog is not None and self.command is None:
            self.permissions[str(self.ctx.guild.id)][self.cog][
                "permissions"
            ] = self.perms
        elif self.cog is not None and self.command is not None:
            if self.cog not in self.permissions[str(self.ctx.guild.id)]:
                self.permissions[str(self.ctx.guild.id)][self.cog] = {}
            if (
                    self.command
                    not in self.permissions[str(self.ctx.guild.id)][self.cog]["commands"]
            ):
                self.permissions[str(self.ctx.guild.id)][self.cog]["commands"][
                    self.command
                ] = {}
            self.permissions[str(self.ctx.guild.id)][self.cog]["commands"][
                self.command
            ]["permissions"] = self.perms
        else:
            print("Cog argument is missing.")
            return
        if write:
            self.write()
        return self

    def write(self):
        try:
            with open("data/permissions.json", "w") as file:
                json.dump(self.permissions, file, indent=4, sort_keys=True)
            self.ctx.cog.bot.permissions = self.permissions
            print("permissions.json saved.")
        except Exception as e:
            print("ERROR saving permissions.json.")
            print(e)

    def clear(self):
        self.perms = {
            "enabled": None,
            "channels_allow_all": None,
            "channels_whitelist": [],
            "channels_blacklist": [],
            "roles_whitelist": [],
            "roles_blacklist": [],
            "users_whitelist": [],
            "users_blacklist": [],
        }
        return self

    def permit(self):
        everyone = self.ctx.guild.default_role.id
        self.perms["enabled"] = True
        self.perms["channels_allow_all"] = True
        self.perms["roles_whitelist"].append(everyone)
        return self

    def restrict(self):
        self.perms["enabled"] = False
        self.perms["channels_allow_all"] = False
        return self


class Emotes:
    def __init__(self, ctx):
        self.bot_emotes = ctx.bot.emotes
        self.emotes = ctx.bot.emotes.get(str(ctx.guild.id), {})
        self.ctx = ctx
        self.defaults = {
            "yes": "🇾",
            "no": "🇳",
            "error": "❌",
        }
        for key in self.defaults.keys():
            if key not in self.emotes:
                self.emotes[key] = self.defaults[key]

    def set(self, name, emote):
        if name in self.defaults:
            self.emotes[name] = emote
            self.save()
            return self

    def reset(self, name):
        self.emotes.pop(name)
        self.save()
        return self

    def save(self):
        self.bot_emotes[str(self.ctx.guild.id)] = self.emotes
        try:
            with open("data/emotes.json", "w") as file:
                json.dump(self.bot_emotes, file, indent=4, sort_keys=True)
            print("emotes.json saved.")
        except Exception as e:
            print("ERROR saving emotes.json.")
            print(e)
