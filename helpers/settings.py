import re
from typing import List

from discord.ext.commands import TextChannelConverter, BadArgument
from discord.ext.commands import Cog, RoleConverter
from discord import User
from discord import TextChannel, user
from hotaru import data
from hotaru import cache
from helpers.mew_helpers import MemberUserConverter
import argparse

settings_list = {}


def invalid_name(name: str):
    if not re.search(r"\s", name):
        if name not in settings_list:
            return None
        else:
            return "invalid setting name, already exists"
    else:
        return "invalid setting name, contains whitespaces"


async def get_all_settings(guild_id):
    settings = {}
    guild_settings_table = await cache.get_table("guild_settings")
    guild_settings = guild_settings_table[guild_id] if guild_id in guild_settings_table else {}
    for setting_name in settings_list:
        if setting_name in guild_settings:
            settings[setting_name] = guild_settings[setting_name]
        else:
            settings[setting_name] = settings_list[setting_name].default_value
    return settings


async def reset_all_settings(guild_id):
    await data.ensure_document("guild_settings", str(guild_id))
    async with data.Connection() as conn:
        await data.r.table("guild_settings").get(str(guild_id)).replace({"id": str(guild_id)}).run(conn)


class GuildSetting:
    type = None
    name = None
    default_value = None

    def __str__(self):
        return str(self.type) + str(self.name)

    async def set_value(self, ctx, *args):
        pass

    async def store_value(self, guild_id, value):
        await data.ensure_document("guild_settings", str(guild_id))
        async with data.Connection() as conn:
            await data.r.table("guild_settings").get(str(guild_id)).update({self.name: value}).run(conn)

    async def reset_value(self, guild_id):
        await data.ensure_document("guild_settings", str(guild_id))
        async with data.Connection() as conn:
            await data.r.table("guild_settings").get(str(guild_id)).replace(data.r.row.without({self.name})).run(conn)

    async def get_value(self, guild_id):
        await data.ensure_document("guild_settings", str(guild_id))
        guild_settings_table = await cache.get_table("guild_settings")
        guild_settings = guild_settings_table[guild_id] if guild_id in guild_settings_table else {}
        if self.name in guild_settings:
            return guild_settings[self.name]
        else:
            return self.default_value

    async def display_value(self, guild_id):
        pass


class BoolSetting(GuildSetting):
    true_display = None
    false_display = None
    arg_err_msg = None
    val_err_msg = None

    def __init__(self, name: str, *,
                 true_display="on",
                 false_display="off",
                 default_value: bool = False,
                 success_msg="{name} is now: {value}",
                 arg_err_msg="do you want it to be {true} or {false}",
                 val_err_msg="{value} is not either {true} or {false}"):

        if not (invalid := invalid_name(name)):
            self.type = bool
            self.name = name
            self.true_display = true_display
            self.false_display = false_display
            self.success_msg = self.replace_vars(success_msg)
            self.arg_err_msg = self.replace_vars(arg_err_msg)
            self.val_err_msg = self.replace_vars(val_err_msg)
            self.default_value = default_value
            settings_list[name] = self
        else:
            print(invalid)

    def replace_vars(self, message: str) -> str:
        msg = re.sub("{name}", str(self.name), message)
        msg = re.sub("{true}", str(self.true_display), msg)
        return re.sub("{false}", str(self.false_display), msg)

    async def set_value(self, ctx, *args):
        if len(args) == 1:
            val = args[0]
            if val == self.true_display:
                await self.store_value(str(ctx.guild.id), True)
                return re.sub("{value}", str(val), self.success_msg)
            elif val == self.false_display:
                await self.store_value(str(ctx.guild.id), False)
                return re.sub("{value}", str(val), self.success_msg)
            else:
                return re.sub("{value}", str(val), self.val_err_msg)
        else:
            if self.arg_err_msg is not None:
                return self.arg_err_msg
            else:
                if self.val_err_msg is not None:
                    return self.val_err_msg
                else:
                    return "give me a valid option, could you?"


class IntSetting(GuildSetting):
    min_val = None
    max_val = None
    success_msg = None
    arg_err_msg = None
    val_err_msg = None
    range_err_msg = None

    def __init__(self, name: str, *,
                 min_val=0,
                 max_val=None,
                 default_value: int = 0,
                 success_msg="{name} is now: {value}",
                 arg_err_msg="this... you need to give me exactly one integer for that one",
                 val_err_msg="this... you need to give me an integer for that one",
                 range_err_msg="{value} is not between {min} and {max}"):
        if not (invalid := invalid_name(name)):
            self.type = int
            self.name = name
            self.default_value = default_value
            self.min_val = min_val
            self.max_val = max_val
            self.success_msg = self.replace_vars(success_msg)
            self.arg_err_msg = self.replace_vars(arg_err_msg)
            self.range_err_msg = self.replace_vars(range_err_msg)
            self.val_err_msg = self.replace_vars(val_err_msg)
            settings_list[name] = self
        else:
            print(invalid)

    def replace_vars(self, message: str) -> str:
        msg = re.sub("{name}", str(self.name), message)
        msg = re.sub("{min}", str(self.min_val) if self.min_val is not None else "-Infinity", msg)
        return re.sub("{max}", str(self.max_val) if self.max_val is not None else "Infinity", msg)

    async def set_value(self, ctx, *args):
        if len(args) == 1:
            try:
                val = int(args[0])
                if not ((self.min_val is None or val < self.min_val) and (self.max_val is None or val > self.min_val)):
                    await self.store_value(str(ctx.guild.id), val)
                    return re.sub("{value}", str(val), self.success_msg)
                else:
                    return re.sub("{value}", str(val), self.range_err_msg)
            except ValueError:
                return self.val_err_msg
        else:
            if self.arg_err_msg is not None:
                return self.arg_err_msg
            else:
                if self.val_err_msg is not None:
                    return self.val_err_msg
                else:
                    return "this... you need to give me an integer for that one"


class FloatSetting(GuildSetting):
    min_val = None
    lopen = True
    max_val = None
    ropen = True
    closed_display = None
    success_msg = None
    arg_err_msg = None
    val_err_msg = None
    range_err_msg = None

    def __init__(self, name: str, *,
                 min_val=0,
                 max_val=None,
                 lopen=True,
                 ropen=True,
                 default_value: float = 0,
                 closed_display=" or equal",
                 success_msg="{name} is now: {value}",
                 arg_err_msg="this... you need to give me exactly one float for that one",
                 val_err_msg="this... you need to give me a float for that one",
                 range_err_msg="{value} is not greater{lopen} than {min} and lesser{ropen} than {max}"):
        if not (invalid := invalid_name(name)):
            self.type = float
            self.name = name
            self.min_val = min_val
            self.max_val = max_val
            self.default_value = default_value
            self.lopen = lopen
            self.ropen = ropen
            self.closed_display = closed_display
            self.success_msg = self.replace_vars(success_msg)
            self.arg_err_msg = self.replace_vars(arg_err_msg)
            self.range_err_msg = self.replace_vars(range_err_msg)
            self.val_err_msg = self.replace_vars(val_err_msg)
            settings_list[name] = self
        else:
            print(invalid)

    def replace_vars(self, message: str) -> str:
        msg = re.sub("{name}", str(self.name), message)
        msg = re.sub("{min}", str(self.min_val) if self.min_val is not None else "-Infinity", msg)
        msg = re.sub("{max}", str(self.max_val) if self.max_val is not None else "Infinity", msg)
        msg = re.sub("{lopen}", "" if self.lopen else str(self.closed_display), msg)
        return re.sub("{ropen}", "" if self.ropen else str(self.closed_display), msg)

    def out_of_range(self, value: float) -> bool:
        greater = False
        lesser = False
        if self.min_val is not None:
            if self.lopen:
                lesser = value <= self.min_val
            else:
                lesser = value < self.min_val
        if self.max_val is not None:
            if self.ropen:
                greater = value >= self.max_val
            else:
                greater = value > self.max_val
        return greater or lesser

    async def set_value(self, ctx, *args):
        if len(args) == 1:
            try:
                val = float(args[0])
                if not (self.out_of_range(val)):
                    await self.store_value(str(ctx.guild.id), val)
                    return re.sub("{value}", str(val), self.success_msg)
                else:
                    return re.sub("{value}", str(val), self.range_err_msg)
            except ValueError:
                return self.val_err_msg
        else:
            if self.arg_err_msg is not None:
                return self.arg_err_msg
            else:
                if self.val_err_msg is not None:
                    return self.val_err_msg
                else:
                    return "this... you need to give me a float for that one"


class ChannelListSetting(GuildSetting):
    add_action = None
    remove_action = None
    add_success_msg = None
    remove_success_msg = None
    add_err_msg = None
    remove_err_msg = None
    arg_err_msg = None
    val_err_msg = None
    action_err_msg = None

    def __init__(self, name: str, *,
                 add_action="add",
                 remove_action="remove",
                 add_success_msg="{channel} has been {add}ed",
                 remove_success_msg="{channel} has been {remove}d",
                 add_err_msg="{channel} was already in the list",
                 remove_err_msg="{channel} wasn't on the list",
                 arg_err_msg="this... you need to give me the action and the channel",
                 val_err_msg="channel not found",
                 action_err_msg="{action} is not either {add} or {remove}"):
        if not (invalid := invalid_name(name)):
            self.type = str(List[TextChannel])
            self.default_value = []
            self.name = name
            self.add_action = add_action
            self.remove_action = remove_action
            self.add_success_msg = self.replace_vars(add_success_msg)
            self.remove_success_msg = self.replace_vars(remove_success_msg)
            self.add_err_msg = self.replace_vars(add_err_msg)
            self.remove_err_msg = self.replace_vars(remove_err_msg)
            self.arg_err_msg = self.replace_vars(arg_err_msg)
            self.val_err_msg = self.replace_vars(val_err_msg)
            self.action_err_msg = self.replace_vars(action_err_msg)
            settings_list[name] = self
        else:
            print(invalid)

    def replace_vars(self, message: str) -> str:
        msg = re.sub("{name}", str(self.name), message)
        msg = re.sub("{add}", str(self.add_action), msg)
        return re.sub("{remove}", str(self.remove_action), msg)

    async def set_value(self, ctx, *args):
        if len(args) == 2:
            if args[0] == self.add_action or args[0] == self.remove_action:
                try:
                    channel_id = str(int(args[1]))
                    channel = None
                except ValueError:
                    try:
                        channel = await TextChannelConverter().convert(ctx, args[1])
                        channel_id = str(channel.id)
                    except BadArgument:
                        return self.val_err_msg
                    #  set value
                channel_name = str(channel.name) if channel is not None else f"id:{channel_id}"
                prev_val = await self.get_value(str(ctx.guild.id))
                if channel_id in prev_val:
                    if args[0] == self.add_action:
                        return re.sub("{channel}", channel_name, self.add_err_msg)
                    else:
                        prev_val.remove(channel_id)
                        await self.store_value(str(ctx.guild.id), prev_val)
                        return re.sub("{channel}", channel_name, self.remove_success_msg)
                else:
                    if args[0] == self.add_action:
                        prev_val.append(channel_id)
                        await self.store_value(str(ctx.guild.id), prev_val)
                        return re.sub("{channel}", channel_name, self.add_success_msg)
                    else:
                        return re.sub("{channel}", channel_name, self.remove_err_msg)
            else:
                return re.sub("{action}", str(args[0]), self.action_err_msg)
        else:
            if self.arg_err_msg is not None:
                return self.arg_err_msg
            else:
                return "this... you need to give me the action and the channel for that one"


class UserListSetting(GuildSetting):
    add_action = None
    remove_action = None
    add_success_msg = None
    remove_success_msg = None
    add_err_msg = None
    remove_err_msg = None
    arg_err_msg = None
    val_err_msg = None
    action_err_msg = None

    def __init__(self, name: str, *,
                 add_action="add",
                 remove_action="remove",
                 add_success_msg="{user} has been {add}ed",
                 remove_success_msg="{user} has been {remove}d",
                 add_err_msg="{user} was already in the list",
                 remove_err_msg="{user} wasn't on the list",
                 arg_err_msg="this... you need to give me the action and the user",
                 val_err_msg="user not found",
                 action_err_msg="{action} is not either {add} or {remove}"):
        if not (invalid := invalid_name(name)):
            self.type = str(List[User])
            self.default_value = []
            self.name = name
            self.add_action = add_action
            self.remove_action = remove_action
            self.add_success_msg = self.replace_vars(add_success_msg)
            self.remove_success_msg = self.replace_vars(remove_success_msg)
            self.add_err_msg = self.replace_vars(add_err_msg)
            self.remove_err_msg = self.replace_vars(remove_err_msg)
            self.arg_err_msg = self.replace_vars(arg_err_msg)
            self.val_err_msg = self.replace_vars(val_err_msg)
            self.action_err_msg = self.replace_vars(action_err_msg)
            settings_list[name] = self
        else:
            print(invalid)

    def replace_vars(self, message: str) -> str:
        msg = re.sub("{name}", str(self.name), message)
        msg = re.sub("{add}", str(self.add_action), msg)
        return re.sub("{remove}", str(self.remove_action), msg)

    async def set_value(self, ctx, *args):
        if len(args) == 2:
            if args[0] == self.add_action or args[0] == self.remove_action:
                try:
                    user_id = str(int(args[1]))
                    usr = None
                except ValueError:

                    usr = await MemberUserConverter().convert(ctx, args[1])
                    if usr.id == 0:
                        return self.val_err_msg
                    else:
                        user_id = str(usr.id)
                    #  set value
                user_name = str(usr.name) if user is not None else f"id:{user_id}"
                prev_val = await self.get_value(str(ctx.guild.id))
                if user_id in prev_val:
                    if args[0] == self.add_action:
                        return re.sub("{user}", user_name, self.add_err_msg)
                    else:
                        prev_val.remove(user_id)
                        await self.store_value(str(ctx.guild.id), prev_val)
                        return re.sub("{user}", user_name, self.remove_success_msg)
                else:
                    if args[0] == self.add_action:
                        prev_val.append(user_id)
                        await self.store_value(str(ctx.guild.id), prev_val)
                        return re.sub("{user}", user_name, self.add_success_msg)
                    else:
                        return re.sub("{user}", user_name, self.remove_err_msg)
            else:
                return re.sub("{action}", str(args[0]), self.action_err_msg)
        else:
            if self.arg_err_msg is not None:
                return self.arg_err_msg
            else:
                return "this... you need to give me the action and the user for that one"


class StringListSetting(GuildSetting):
    add_action = None
    remove_action = None
    add_success_msg = None
    remove_success_msg = None
    add_err_msg = None
    remove_err_msg = None
    arg_err_msg = None
    val_err_msg = None
    action_err_msg = None

    def __init__(self, name: str, *,
                 add_action="add",
                 remove_action="remove",
                 add_success_msg="{word} has been {add}ed",
                 remove_success_msg="{word} has been {remove}d",
                 add_err_msg="{word} was already in the list",
                 remove_err_msg="{word} wasn't on the list",
                 arg_err_msg="this... you need to give me the action and the word",
                 val_err_msg="invalid string",
                 action_err_msg="{action} is not either {add} or {remove}"):
        if not (invalid := invalid_name(name)):
            self.type = str(List[str])
            self.default_value = []
            self.name = name
            self.add_action = add_action
            self.remove_action = remove_action
            self.add_success_msg = self.replace_vars(add_success_msg)
            self.remove_success_msg = self.replace_vars(remove_success_msg)
            self.add_err_msg = self.replace_vars(add_err_msg)
            self.remove_err_msg = self.replace_vars(remove_err_msg)
            self.arg_err_msg = self.replace_vars(arg_err_msg)
            self.val_err_msg = self.replace_vars(val_err_msg)
            self.action_err_msg = self.replace_vars(action_err_msg)
            settings_list[name] = self
        else:
            print(invalid)

    def replace_vars(self, message: str) -> str:
        msg = re.sub("{name}", str(self.name), message)
        msg = re.sub("{add}", str(self.add_action), msg)
        return re.sub("{remove}", str(self.remove_action), msg)

    async def set_value(self, ctx, *args):
        if len(args) == 2:
            if args[0] == self.add_action or args[0] == self.remove_action:
                word = args[1]
                prev_val = await self.get_value(str(ctx.guild.id))
                if word in prev_val:
                    if args[0] == self.add_action:
                        return re.sub("{word}", word, self.add_err_msg)
                    else:
                        prev_val.remove(word)
                        await self.store_value(str(ctx.guild.id), prev_val)
                        return re.sub("{word}", word, self.remove_success_msg)
                else:
                    if args[0] == self.add_action:
                        prev_val.append(word)
                        await self.store_value(str(ctx.guild.id), prev_val)
                        return re.sub("{word}", word, self.add_success_msg)
                    else:
                        return re.sub("{word}", word, self.remove_err_msg)
            else:
                return re.sub("{action}", str(args[0]), self.action_err_msg)
        else:
            if self.arg_err_msg is not None:
                return self.arg_err_msg
            else:
                return "this... you need to give me the action and the word for that one"



