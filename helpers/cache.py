"""the cache module caches requested tables for faster access to them"""
from asyncio import Lock
import asyncio
from rethinkdb.errors import ReqlOpFailedError


class Cache:

    def __init__(self):
        self.cached_tables = []
        self.local_cache = {}
        self.cache_lock = Lock()
        self.cache_task = None
        self.respawn_task = False

    async def get_table(self, table_name: str) -> dict:
        """
        gets a table from the cache, if the table is not in the cache, it caches it and returns it's current value
        :param table_name:
        :return:
        """
        await self.cache_lock.acquire()  # acquires a lock to avoid race conditions on the local cache
        if table_name not in self.cached_tables:  # if the table is not cached
            from hotaru import data
            conn = await data.conn()
            await data.ensure_table(table_name)  # makes sure the requested table exists
            self.cached_tables.append(table_name)  # adds the table to the list of cached tables
            if self.cache_task is None:  # if the cache task was started before, cancel it before running it again
                self.cache_task = asyncio.create_task(self.listener_thread(), name='cache')
            else:
                self.respawn_task = True
                self.cache_task.cancel()
            table_data = {}  # initializes the copy of the table that will both go into the cache and be returned
            async for document in await data.r.table(table_name).run(conn):  # fills the local copy of the table
                table_data[document['id']] = document
            self.local_cache[table_name] = table_data  # initializes the copy in the cache with the received data
            self.cache_lock.release()  # releases the lock
            await conn.close()
            return table_data
        else:
            table_data = self.local_cache[table_name]  # if the table is in the cache, copy it and release the lock
            self.cache_lock.release()
            return table_data

    async def listener_thread(self):
        from hotaru import data
        self.respawn_task = False
        conn = await data.conn()
        try:
            if len(self.cached_tables) > 0:  # initializes the changefeed to subscribe to
                feed = data.r.table(self.cached_tables[0]).merge({'table': self.cached_tables[0]})
                if len(self.cached_tables) > 1:
                    for table in self.cached_tables[1:]:  # adds the previous changefeed to the existing one
                        feed = feed.union(data.r.table(table).merge({'table': table}))
                feed = await feed.changes().run(conn)  # gets the feed that will listen for changes on cached tables
                async for change in feed:
                    await self.cache_lock.acquire()  # gets a lock on the cache to avoid race conditions
                    if change['new_val'] is not None:  # if the document was not deleted
                        document = change['new_val']
                        table_name = change['new_val']['table']  # gets the name of the table from the change
                        if table_name not in self.local_cache:  # probably unnecessary safety check
                            self.local_cache[table_name] = {}
                        if change['new_val']['id'] not in self.local_cache[table_name]:  # creates the document if
                            self.local_cache[table_name][change['new_val']['id']] = {}  # it was added by the change
                        del document['table']  # removes the auxiliary field 'table' from the received document
                        self.local_cache[table_name][change['new_val']['id']] = document  # updates the cache
                    else:
                        table_name = change['old_val']['table']  # if the document was deleted
                        if change['old_val']['id'] in self.local_cache[table_name]:  # remove it from the local copy
                            del self.local_cache[table_name][change['old_val']['id']]
                    self.cache_lock.release()  # releases the cache lock to allow reading the cache
        except ReqlOpFailedError:  # if a table cached is dropped, remove it from the cached tables and restart the task
            cached_tables = set(self.cached_tables)
            self.cached_tables = list(cached_tables - (cached_tables - set(await data.r.table_list().run(conn))))
            self.cache_task.cancel()
            self.cache_task = asyncio.create_task(self.listener_thread(), name='cache')
        except asyncio.CancelledError:
            if self.respawn_task:
                self.cache_task = asyncio.create_task(self.listener_thread(), name='cache')
            raise  # rethrows the exception so that the task gets cancelled
        finally:
            await conn.close(noreply_wait=False)  # forces the connection to close, to allow the listener to end
