import discord
from discord import AllowedMentions, Embed


async def embed_message(channel, content):
    embed = discord.Embed(description=content, color=0x00ff00)
    await channel.send(embed=embed)


async def embed_embed(channel, embed):
    await channel.send(embed=embed)


async def code_block_message(channel, content):
    await channel.send(content, allowed_mentions=AllowedMentions.none())


async def code_block_embed(channel, embed):
    content = "```"
    if embed.title != Embed.Empty:
        content += f"{embed.title}\n"
    if embed.description != Embed.Empty:
        content += f"{embed.description}\n"
    for field in embed.fields:
        content += f"{field.name}\n"
        content += f"\t{field.value}\n"
    content += "```"
    await channel.send(content, allowed_mentions=AllowedMentions.none())


class Messages:
    def __init__(self, backend: str = "embed"):
        if backend == "code_block":
            self.backend = backend
        else:
            self.backend = "embed"

    async def send_message(self, channel, content):
        if self.backend == "embed":
            await embed_message(channel, content)
        elif self.backend == "code_block":
            await code_block_message(channel, content)

    async def send_embed(self, channel, embed):
        if self.backend == "embed":
            await embed_embed(channel, embed)
        elif self.backend == "code_block":
            await code_block_embed(channel, embed)
