import re
from typing import Tuple
import discord
from discord.ext import commands

char_replacements = {'#': 'h', '$': 's', '(': 'l', ')': 'l', '+': 't', '/': 'l', '0': 'o', '1': 'l', '2': 'z', '3': 'e',
                     '4': 'a', '5': 's', '6': 'g', '7': 't', '8': 'b', '9': 'g', '@': 'a', '|': 'l'}


class MemberUserConverter(commands.Converter):
    async def convert(self, ctx, argument):
        try:
            usr = (await commands.MemberConverter.convert(commands.MemberConverter(), ctx, argument))
        except commands.BadArgument:
            try:
                usr = (await commands.UserConverter.convert(commands.UserConverter(), ctx, argument))
            except commands.BadArgument:
                await ctx.send(embed=discord.Embed(description="user not found", color=0x00ff00),
                               delete_after=5)
                usr = ctx.bot.user
                usr.id = 0
        return usr


class EmojiConverter(commands.Converter):
    async def convert(self, ctx, argument):
        try:
            emoji = (await commands.PartialEmojiConverter().convert(ctx, argument))
        except commands.BadArgument:
            emoji = discord.PartialEmoji(name=argument)
        return emoji


def is_command(bot, msg: discord.message) -> bool:
    """
    a small utility method to test if the message tried to invoke a command, regardless of if it will fail or not
    :param bot: should be the bot to check for
    :rtype: bool
    :param msg: the message to check
    :return: a bool, True if this message is a command
    """
    if msg.content.startswith(prefix := bot.command_prefix):  # check if the prefix is the bot's prefix
        invoked_command = msg.content.partition(' ')[0].partition(prefix)[2]
        for command in bot.commands:
            if invoked_command == command.name or invoked_command in command.aliases:
                return True  # checks all commands and aliases, to see is the message's content matches
    else:
        return False


async def is_spam(msg: str, guild=None):
    """
    stub method, will be used to test if a message is spam
    :param msg:
    :param guild:
    :return:
    """

    return False


def remove_whitespace(text: str) -> str:
    """

    :rtype: str
    :param text: the string to remove whitespace from
    :return: the string without whitespace
    """
    return re.sub(r'\s+', '', text)


def count_words(text: str) -> int:
    """

    :rtype: int
    :param text: the text to count the words in
    :return: the number of words in the text
    """
    return len(re.findall(r'\w+', text))


def remove_channel_mentions(text: str) -> Tuple[str, int]:
    """

    :rtype: (str, int)
    :param text:
    :return:
    """
    return re.subn(r'<#\d+>', '', text)


def remove_role_mentions(text: str) -> Tuple[str, int]:
    """

    :param text:
    :return:
    :rtype: (str, int)
    """
    return re.subn(r'<@&\d+>', '', text)


def remove_user_mentions(text: str) -> Tuple[str, int]:
    """

    :rtype: (str, int)
    :param text:
    :return:
    """
    return re.subn(r'<(@!?)\d+>', '', text)


def remove_custom_emojis(text: str) -> Tuple[str, int]:
    """

    :rtype: (str, int)
    :param text:
    :return:
    """
    return re.subn(r'<a?:[^:]{2,}:\d+>', '', text)


def remove_links(text: str) -> Tuple[str, int]:
    """

    :rtype: (str, int)
    :param text:
    :return:
    """
    return re.subn(r'https?://(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_+.~#?&/=]{0,256})',
                   '', text)
