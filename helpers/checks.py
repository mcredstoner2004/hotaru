from discord.ext import commands
from helpers.objects import Permissions


def permissions_check(ctx):
    # Checks for whether the command may run for that ctx.guild.
    # Allows commands with no ctx.guild so that DM commands can work.
    # TODO: Migrate permissions.json to the database.
    if ctx.guild is None:
        return True
    if ctx.cog is None:
        return True
    if ctx.cog.qualified_name.lower() in ["basics", "configure", "manage"]:
        return True
    cog_permissions = Permissions(ctx, ctx.cog.qualified_name.lower()).load()
    command_permissions = Permissions(
        ctx, ctx.cog.qualified_name.lower(), ctx.command.name
    ).load()

    if command_permissions.perms["enabled"] is not None:
        return command_permissions.perms["enabled"]
    else:
        return cog_permissions.perms["enabled"]


def allowed_channel():
    async def predicate(ctx):
        if ctx.guild is None:
            return False
        if ctx.cog is None:
            return False
        if ctx.author.guild_permissions.manage_guild:
            return True
        cog_permissions = Permissions(ctx, ctx.cog.qualified_name.lower()).load()
        command_permissions = Permissions(
            ctx, ctx.cog.qualified_name.lower(), ctx.command.name
        ).load()
        if ctx.channel.id in command_permissions.perms["channels_blacklist"]:
            return False
        elif ctx.channel.id in command_permissions.perms["channels_whitelist"]:
            return True
        elif ctx.channel.id in cog_permissions.perms["channels_blacklist"]:
            return False
        elif ctx.channel.id in cog_permissions.perms["channels_whitelist"]:
            return True
        elif command_permissions.perms["channels_allow_all"] is not None:
            return command_permissions.perms["channels_allow_all"]
        else:
            return cog_permissions.perms["channels_allow_all"]

    return commands.check(predicate)


def server_manager_or_perms():
    async def predicate(ctx):
        if ctx.guild is None:
            return False
        if ctx.cog is None:
            return False
        if ctx.author.guild_permissions.manage_guild:
            return True

        cog_permissions = Permissions(ctx, ctx.cog.qualified_name.lower()).load()
        command_permissions = Permissions(
            ctx, ctx.cog.qualified_name.lower(), ctx.command.name
        ).load()

        if ctx.author.id in command_permissions.perms["users_blacklist"]:
            return False
        if ctx.author.id in command_permissions.perms["users_whitelist"]:
            return True
        if ctx.author.id in cog_permissions.perms["users_blacklist"]:
            return False
        if ctx.author.id in cog_permissions.perms["users_whitelist"]:
            return True

        guild_roles = [role.id for role in ctx.guild.roles]
        guild_roles.reverse()
        user_roles = {role.id for role in ctx.author.roles}
        matches = {}

        def set_matcher(perm, state):
            roles = [match for match in (user_roles & set(perm))]
            for role in roles:
                matches[role] = state

        set_matcher(command_permissions.perms["roles_whitelist"], True)
        set_matcher(command_permissions.perms["roles_blacklist"], False)
        if len(matches) == 0:
            set_matcher(cog_permissions.perms["roles_whitelist"], True)
            set_matcher(cog_permissions.perms["roles_blacklist"], False)

        for role in guild_roles:
            if role in matches:
                return matches[role]

        return False

    return commands.check(predicate)
