import discord
import json


def set_intents():
    # Prepare Intents object.
    print("Preparing intents")
    intents = discord.Intents.none()
    intents.guilds = True
    intents.members = True
    intents.emojis = True
    intents.presences = True
    intents.messages = True
    intents.reactions = True
    return intents


def get_settings(setting=None):
    # Check for a settings.json file that has Hotaru's settings.
    try:
        with open("settings.json", "r") as file:
            settings = json.load(file)
        if setting is not None:
            return settings[setting]
        else:
            return settings

    except Exception as e:
        print(e)
        print("No settings.json present")
        exit()


def get_permissions():
    try:
        with open("data/permissions.json", "r") as file:
            permissions = json.load(file)
            return permissions
    except Exception as e:
        print(e)
        print("No data/permissions.json present")
        return {}


def get_emotes():
    try:
        with open("data/emotes.json", "r") as file:
            emotes = json.load(file)
            return emotes
    except Exception as e:
        print(e)
        print("No data/emotes.json present")
        return {}
