"""the data module is responsible for RethinkDB interactions"""

from discord.ext import commands
from rethinkdb import RethinkDB


class Data:
    def __init__(self, server):
        self.r = RethinkDB()
        self.r.set_loop_type("asyncio")
        self.server = server
        self.Connection = Connection
        self.Connection.data = self

    async def conn(self, db: str = None):
        """The connection information for the database."""

        if db is None:
            db = self.server["db"]

        return await self.r.connect(self.server["host"], self.server["port"], db)

    async def check_tables(self, bot):
        """Check for and generate tables in the database if missing."""
        print("Checking database...")
        conn = await self.r.connect(
            self.server["host"],
            self.server["port"],
        )
        db = await self.r.db_list().run(conn)
        if "hotaru" not in db:
            await self.r.db_create("hotaru").run(conn)
            print("Database hotaru created.")
        for guild in bot.guilds:
            if str(guild.id) not in db:
                await self.r.db_create(str(guild.id)).run(conn)
                print(f"Database for {guild.name} created.")
        await conn.close()

        main_tables = ["config", "user_data"]

        guild_tables = ["user_info", "help_info", "ignore_list"]

        conn = await self.conn()
        print("Checking tables...")
        tables = await self.r.table_list().run(conn)

        for table in main_tables:
            if table not in tables:
                print(f"Table {table} not found, creating...")
                await self.r.table_create(table).run(conn)
                print(f"Table {table} created.")
        await conn.close()
        for guild in bot.guilds:
            conn = await self.conn(str(guild.id))
            tables = await self.r.table_list().run(conn)
            for table in guild_tables:
                if table not in tables:
                    print(f"Table {table} not found, creating...")
                    await self.r.table_create(table).run(conn)
                    print(f"Table {table} created.")
            await conn.close()


        print("Table check complete.")

    async def ensure_table(self, table_name: str) -> None:
        """after this function returns, it's sure to read the table required
        :param table_name: the table to ensure
        :rtype: None
        """
        async with self.Connection() as conn:
            await self.r.table_list().contains(table_name).do(
                lambda table_exists: self.r.branch(table_exists, (), self.r.table_create(table_name))
            ).run(conn)

    async def ensure_document(self, table_name: str, document_name: str) -> None:
        """after this function returns, it's sure to read the table and the document requested
                :param document_name: the document to ensure
                :param table_name: the table to ensure
                :rtype: None
                """
        async with self.Connection() as conn:
            await self.r.table_list().contains(table_name).do(
                lambda table_exists: self.r.branch(table_exists, (), self.r.table_create(table_name))
            ).run(conn)
            await self.r.table(table_name).get(document_name).do(
                lambda user_data: self.r.branch(user_data, {}, self.r.table(table_name).insert({
                    "id": document_name
                }))
            ).run(conn)

    async def ensure_table_index(self, table_name: str, index_name: str):
        async with self.Connection() as conn:
            await self.r.table(table_name).index_list().contains(index_name).do(
                lambda index_exists: self.r.branch(index_exists, {}, self.r.table(table_name).index_create(index_name))
            ).run(conn)
            await self.r.table(table_name).index_wait(index_name).run(conn)


class Connection:
    data = None

    def __init__(self, db: str = None):
        self.conn = None
        self.db = db

    async def __aenter__(self):
        self.conn = await self.data.conn(self.db)
        return self.conn

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.conn.close()
        return False
