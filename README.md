Hotaru is Celvice's hobby project bot.

She began as a simple exercise in storing and repeating help messages,
but now she'll be an exercise in OOP and modularization.

HOW TO USE:

1. Install all of Hotaru's requirements from requirements.txt

    py -3 -m pip install -r requirements.txt -U

2. Create your settings.json using the example_settings.txt as a template.

3. Make sure you have a bot for testing, and create its token.json using the
    instructions inside example_settings.txt.

4. Make sure you have RethinkDB set up and already running. If you're running it
    on another machine in the network, make sure you started RethinkDB binded:

    rethinkdb --bind all

    Otherwise, it'll only listen for connections on the localhost.