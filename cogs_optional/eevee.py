"""The Eevee cog represents functionality exclusive to the Eevee Save Project."""


from helpers.objects import Emotes
from discord.ext import commands
from helpers.checks import allowed_channel, server_manager_or_perms
from helpers.helpers import get_settings
import asyncio
import datetime
import difflib
import discord
import inflect
import json
import math
import pandas as pd
import re
from hotaru import data


def is_esp():
    """Decorator check for the ESP server."""
    # TODO: Generalize the server checks to refer to a master permissions list.
    def predicate(ctx):
        eevee_list = get_settings("eevee_list")
        return ctx.guild.id in eevee_list

    return commands.check(predicate)


def get_icons():
    """Check data/titles.json and return a name: icon_url dictionary."""
    titles = {}

    try:
        with open("data/titles.json", "r") as f:
            titles = json.load(f)

    except Exception as e:
        print(e)
        print("Error opening titledb.")

    icons = {}
    for title in titles.values():
        if title["name"] is not None:
            icons[title["id"]] = {"name": title["name"], "url": title["iconUrl"]}

    return icons


class Eevee(commands.Cog):
    def __init__(self, bot):
        # TODO: Move eevee links to self.bot, only refresh when a new XLSX is presented.
        self.bot = bot
        self.eevee = pd.read_csv("data/ESPspreadsheet2.0.csv")
        self.icons = get_icons()
        self.titles = []
        for value in self.icons.values():
            self.titles.append(value["name"])

    @commands.command()
    @is_esp()
    @server_manager_or_perms()
    @allowed_channel()
    async def upload(self, ctx, filename, *, text):
        file = ctx.message.attachments[0]
        fields = [x.start() for x in re.finditer(r"\[", filename)]
        title = filename[0 : fields[0]].strip()
        description = filename[fields[0] + 1 : fields[-3] - 2].strip()
        cid = filename[fields[-2] : fields[-1]].strip(" []")
        sid = filename[fields[-1] :].partition(".")[0].strip(" []")
        converted = await file.to_file()
        embed = discord.Embed(
            color=0x00FF00,
            description=(
                f"**Title:** {title}\n"
                f"**TitleID:** {cid}\n"
                f"**SaveID:** {sid}\n"
                f"**Summary:** {description}\n"
                f"**Description:**\n{text}"
            ),
        )
        await ctx.channel.send(embed=embed, file=converted)

    @commands.Cog.listener("on_message")
    async def upload_listener(self, message):
        if message.channel.id == 797723089873272842:
            if len(message.attachments) == 1:
                ctx = await self.bot.get_context(message)
                title = message.content.partition("\n")[0].strip()
                text = message.content.partition("\n")[2].strip()
                await self.upload(ctx, title, text=text)

    @commands.group()
    @commands.guild_only()
    @is_esp()
    @allowed_channel()
    async def save(self, ctx):
        """The main group for save functionality."""
        basics = self.bot.get_cog("Basics")

        if ctx.invoked_subcommand is None:
            await ctx.message.delete()
            embed = discord.Embed(
                color=0x00FF00,
                description=(
                    "Looking for something?\n"
                    f"You can browse the entire catalogue with `{self.bot.command_prefix}save browse`,\n"
                    f"you can search for a title with `{self.bot.command_prefix}save search <query>`,\n"
                    f"you can get a description with `{self.bot.command_prefix}save info <number>`,\n"
                    f"and you can grab the link with `{self.bot.command_prefix}save request <number>`.\n"
                    f"You can collect stamps with `{self.bot.command_prefix}peel`,\n"
                    f"and check your stamp collection with `{self.bot.command_prefix}stamps`."
                ),
            )

            if await self.bot.is_owner(ctx.message.author) is True:
                await ctx.send(embed=embed)
            else:
                await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=30)

    @save.command()
    async def browse(self, ctx, page: int = None):
        """Browse the saves catalogue.

        Press the reacts to tab through the pages.

        Args:
            page (str, optional): The page to start on. Defaults to None.
        """
        # TODO: Error handler for INT conversion failed

        basics = self.bot.get_cog("Basics")

        titles = self.eevee[["Name", "Description (Summary)"]]
        reacts = ["⏬", "⬅️", "➡️", "⏫"]
        pages = math.floor(len(titles) / 10)

        def generate_page():
            for entry in titles.iloc[
                page_number * 10 : (page_number + 1) * 10
            ].iterrows():
                embed.add_field(
                    name=entry[1][0], value=f"`{entry[0]}`: {entry[1][1]}", inline=False
                )

        def check(reaction, user):
            return (
                user == ctx.author
                and str(reaction.emoji) in reacts
                and reaction.message.id == sent.id
            )

        await ctx.message.delete()

        if page in range(0, pages):
            page_number = page
        else:
            page_number = 0
        embed = discord.Embed(color=0x00FF00, title="**ESP Catalog**")
        generate_page()

        sent = await ctx.send(f"<@{str(ctx.author.id)}>", embed=embed)
        for react in reacts:
            await sent.add_reaction(react)

        while True:
            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", timeout=30, check=check
                )
                # Change the page_number while making sure not to go out of range.
                # ? I don't know how to have the bot wait for either reaction add or remove.
                await sent.remove_reaction(reaction, user)
                if str(reaction.emoji) == reacts[0]:
                    if page_number - 10 >= 0:
                        page_number -= 10
                    else:
                        page_number = 0
                elif str(reaction.emoji) == reacts[1]:
                    if page_number - 1 >= 0:
                        page_number -= 1
                    else:
                        page_number = 0
                elif str(reaction.emoji) == reacts[2]:
                    if page_number + 1 <= pages:
                        page_number += 1
                    else:
                        page_number = pages
                else:
                    if page_number + 10 <= pages:
                        page_number += 10
                    else:
                        page_number = pages
                embed.clear_fields()
                generate_page()
                await sent.edit(embed=embed)
            except:  # TODO make except more specific
                # // await sent.clear_reactions()
                await sent.delete()
                return

    @browse.error
    async def browse_error(self, ctx, e):
        if isinstance(e, commands.BadArgument):
            await self.browse(ctx)

    @save.command(aliases=["find"])
    async def search(self, ctx, *, query: str = None):
        """Search for saves by title.

        This will fail if there enough results to overfill the embed description.

        Args:
            query (str, optional): A string to match titles against. Defaults to None.
        """

        basics = self.bot.get_cog("Basics")

        await ctx.message.delete()

        results = self.eevee[self.eevee.Name.str.contains(query, case=False, na=False)]

        if len(results) == 0:
            embed = discord.Embed(
                color=0x00FF00,
                description=(
                    "I couldn't find a match for that, sorry.\n" "~~_Not sorry_~~"
                ),
            )
            await ctx.send(f"<@{str(ctx.author.id)}>", embed=embed, delete_after=10)
            return
        elif len(results) > 12:
            embed = discord.Embed(
                color=0x00FF00,
                description=(
                    "That's, uh, kinda broad.\n"
                    "Could you search for something more specific?"
                ),
            )
            sent = await ctx.send(f"<@{str(ctx.author.id)}>", embed=embed, delete_after=10)
            return

        embed = discord.Embed(color=0x00FF00, title="**Search results:**")

        for title in results.iterrows():
            embed.add_field(
                name=title[1][2], value=f"`{title[0]}`: {title[1][3]}", inline=False
            )

        await ctx.send(f"<@{str(ctx.author.id)}>", embed=embed, delete_after=30)

    @save.command(aliases=["description", "describe", "desc"])
    async def info(self, ctx, query: int = -1):
        """Request the full save description.

        It also embeds a thumbnail of the game.

        Args:
            query (int, optional): The save index number. Defaults to None.
        """

        emotes = Emotes(ctx)

        if query < 0:
            await ctx.message.add_reaction(emotes.emotes["error"])
            await asyncio.sleep(10)
            await ctx.message.delete()
            return

        def check(reaction, user):
            return (
                user == ctx.author
                and str(reaction.emoji) == emotes.emotes["yes"]
                and reaction.message.id == sent.id
            )

        await ctx.message.delete()

        response = self.eevee.iloc[query]
        thumbnail = ""
        if response["Title ID"] in self.icons:
            thumbnail = self.icons[response["Title ID"]]["url"]

        if (
            isinstance(response["Description (Full)"], str) is False
            and response["Description (Summary)"] is not None
        ):
            response["Description (Full)"] = (
                f"\*Full description not available.\*\n\n"
                f"{response['Description (Summary)']}"
            )

        else:
            matches = difflib.get_close_matches(response["Name"], self.titles)
            if len(matches) != 0:
                content_id = ""
                for key, value in self.icons.items():
                    if matches[0] == value["name"]:
                        content_id = key
                thumbnail = self.icons[content_id]["url"]

        embed = discord.Embed(
            color=0x00FF00,
            title=response["Name"],
            description=(
                response["Description (Full)"]
                + "\n\n**Click on the __yes__ react below to request this save.**"
            ),
        )

        embed.set_thumbnail(url=thumbnail)

        sent = await ctx.send(f"<@{str(ctx.author.id)}>", embed=embed)
        await sent.add_reaction(emotes.emotes["yes"])

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", timeout=30, check=check
            )
            # // await sent.clear_reactions()
            await sent.delete()
            await self.request(ctx, query)
        except:  # TODO make except more specific
            # // await sent.clear_reactions()
            await sent.delete()

    @info.error
    async def info_error(self, ctx, e):
        if isinstance(e, commands.BadArgument):
            basics = self.bot.get_cog("Basics")
            await ctx.message.delete()
            embed = discord.Embed(
                color=0x00FF00,
                description=(
                    "You have to use the index number.\n"
                    "Try looking it up with `browse` or `search`."
                ),
            )
            await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)

    @save.command(aliases=["req", "link", "url"])
    async def request(self, ctx, req: int = -1):
        """Request a link to the save.

        Args:
            req (int, optional): The save index number. Defaults to None.
        """
        emotes = Emotes(ctx)
        if req < 0:
            await ctx.message.add_reaction(emotes.emotes["error"])
            await asyncio.sleep(10)
            await ctx.message.delete()
            return

        basics = self.bot.get_cog("Basics")
        bank = self.bot.get_cog("Bank")
        p = inflect.engine()
        emotes = Emotes(ctx)

        def check(reaction, user):
            return (
                user == ctx.author
                and str(reaction.emoji) in reacts
                and reaction.message.id == sent.id
            )

        try:
            await ctx.message.delete()
        except:  # TODO make except more specific
            pass

        await bank.check_user(ctx.author)
        async with data.Connection() as conn:
            blue = await data.r.table("user_data").get(ctx.author.id)["blue"].run(conn)

        if blue < 3:
            embed = discord.Embed(
                color=0x00FF00,
                description=(
                    "Looks like you don't have enough stamps.\n"
                    "You need at least three to make a request."
                ),
            )
            await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)
            return

        response = self.eevee.iloc[req]
        if isinstance(response["File ID (Hotaru)"], str) is False:
            embed = discord.Embed(
                color=0x00FF00,
                description=(
                    "Odd... looks like I'm missing the link for that one, sorry."
                ),
            )
            await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)
            return

        embed = discord.Embed(
            color=0x00FF00,
            title=response["Name"],
            description=(
                "You're requesting this save now for 3 stamps.\n"
                "Make sure you have DMs enabled!\n\n"
                f"Are you sure? You currently have {blue} stamps."
                "**Click on the __yes__ react to confirm.**"
            ),
        )

        sent = await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed)

        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]

        for react in reacts:
            await sent.add_reaction(react)

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", timeout=30, check=check
            )
            await sent.clear_reactions()

            if str(reaction.emoji) == reacts[0]:
                await sent.delete()
                await bank.withdraw(ctx.author, 3, "blue")
                snip = "https://drive.google.com/uc?id=" + response["File ID (Hotaru)"]
                embed.description = response["Description (Summary)"]
                embed.add_field(name="Link", value=snip, inline=False)
                reply = discord.Embed(
                    color=0x00FF00,
                    description=(
                        f"Okay, you have {blue - 3} {p.plural('stamp', (blue - 3))} left. "
                        + "Check your DMs."
                    ),
                )
                await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=reply, delete_after=10)
                await ctx.author.send(embed=embed)
            else:
                await sent.delete()
                await basics.embed(ctx.channel, "Okay, nevermind.", delete_after=10)
        except:  # TODO make except more specific
            # // await sent.clear_reactions()
            await sent.delete()

    @request.error
    async def request_error(self, ctx, e):
        # TODO: Move to decorator.
        if isinstance(e, commands.CheckFailure):
            basics = self.bot.get_cog("Basics")
            await basics.embed(ctx.channel, "This doesn't look like ESP...")
        elif isinstance(e, commands.BadArgument):
            basics = self.bot.get_cog("Basics")
            await ctx.message.delete()
            embed = discord.Embed(
                color=0x00FF00,
                description=(
                    "You have to use the index number.\n"
                    "Try looking it up with `browse` or `search`."
                ),
            )
            await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)

    @commands.command()
    @is_esp()
    @allowed_channel()
    async def peel(self, ctx):
        """Basic ESP economy command."""
        # TODO: Comment the history checks.
        basics = self.bot.get_cog("Basics")
        bank = self.bot.get_cog("Bank")
        p = inflect.engine()

        reacts = ["0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"]

        await ctx.message.delete()
        await bank.check_user(ctx.author)
        async with data.Connection() as conn:
            try:
                last_peel = (
                    await data.r.table("user_data")
                    .get(ctx.author.id)["last_peel"]
                    .run(conn)
                )
                delta_peel = (
                    ctx.message.created_at.astimezone(data.r.make_timezone("00:00"))
                    - last_peel
                )

            except:  # TODO make except more specific
                last_peel = None
                delta_peel = None

            if last_peel is not None:
                try:
                    # TODO: Implement botting detection.
                    # history = await data.r.table('user_data').get(ctx.author.id)['peel_history'].run(conn)
                    pass
                except:  # TODO make except more specific
                    pass

            if last_peel is None or delta_peel > datetime.timedelta(hours=1):
                if last_peel is None:
                    stamps = 5
                else:
                    stamps = int(delta_peel / datetime.timedelta(hours=1))
                    if stamps > 5:
                        stamps = 5
                await bank.deposit(ctx.author, stamps, "blue")
                this_peel = data.r.expr(
                    ctx.message.created_at.astimezone(data.r.make_timezone("00:00"))
                )
                await data.r.table("user_data").get(ctx.author.id).update(
                    {"last_peel": this_peel}
                ).run(conn)

                keys = await data.r.table("user_data").get(ctx.author.id).keys().run(conn)

                if "peel_history" in keys:
                    if (
                        await data.r.table("user_data")
                        .get(ctx.author.id)["peel_history"]
                        .run(conn)
                        is not None
                    ):
                        await data.r.table("user_data").get(ctx.author.id).update(
                            {
                                "peel_history": data.r.row["peel_history"].append(
                                    (this_peel, delta_peel.total_seconds())
                                )
                            }
                        ).run(conn)
                else:
                    await data.r.table("user_data").get(ctx.author.id).update(
                        {"peel_history": [(this_peel, 0)]}
                    ).run(conn)

                blue = await data.r.table("user_data").get(ctx.author.id)["blue"].run(conn)
                embed = discord.Embed(
                    color=0x00FF00,
                    description=(
                        f'{ctx.author.display_name} peeled off ' 
                        f'{stamps} {p.plural("stamp", stamps)} for their collection.\n'
                        f'{ctx.author.display_name} now has {blue} {p.plural("stamp", blue)}.'
                    ),
                )
                await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)
                # //await ctx.message.add_reaction('💌')
                # //await ctx.message.add_reaction(reacts[stamps])

            else:
                time_left = datetime.timedelta(hours=1) - delta_peel
                minutes_left = math.ceil(time_left / datetime.timedelta(minutes=1))
                tens = int(minutes_left / 10)
                ones = int(minutes_left % 10)
                embed = discord.Embed(
                    color=0x00FF00,
                    description=(
                        f"Looks like there aren't any postcards for you.\n"
                        f"A new postcard will arrive in {minutes_left} {p.plural('minute', minutes_left)}."
                    ),
                )
                await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)
                # //await ctx.message.add_reaction('⏲️')
                # //await ctx.message.add_reaction(reacts[tens])
                # //await ctx.message.add_reaction(reacts[ones])

    @commands.Cog.listener("on_command_error")
    async def dict_checker(self, ctx, e):
        """The listener that calls the above dictionary checks."""
        if ctx.channel.id == 760419063351869460:
            if isinstance(e, (commands.CommandNotFound, commands.CheckFailure)):
                await ctx.message.delete()
                embed = discord.Embed(
                    color=0x00FF00,
                    description=(
                        "Looking for something? Try `+save` for the help message."
                    ),
                )
                sent = await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)

    @commands.Cog.listener("on_message")
    async def kitchen_cleaner(self, message):
        """Deletes any messages in #takeout-menu that aren't commands"""
        if message.channel.id == 760419063351869460:
            if (
                    not message.content.startswith(self.bot.command_prefix) and not message.author.bot) or (
                    message.content.startswith(self.bot.command_prefix + " ")):
                await message.delete()
                embed = discord.Embed(
                    color=0x00FF00,
                    description=(
                        "Looking for something? Try `+save` for the help message."
                    ),
                )
                await message.channel.send(f"<@{str(message.author.id)}>\n", embed=embed, delete_after=10)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def rebirth_check(self, ctx, member: int):
        basics = self.bot.get_cog("Basics")

        guild = self.bot.get_guild(685228508892626971)
        try:
            rb_member = guild.get_member(member)
            roles = [role.name for role in rb_member.roles]
            time = datetime.datetime.now() - rb_member.joined_at
            await basics.embed(
                ctx.channel,
                f"{rb_member.display_name} joined RB {abs(int(time.total_seconds() // 86400))} days"
                f", and {abs(int(time.total_seconds() % 86400 // 3600))} hours ago.\n"
                f"Roles: {', '.join(roles)}",
            )
        except Exception as e:
            await basics.embed(
                ctx.channel,
                f"Error: {e}\n" f"That person doesn't appear to be a member of RB.",
            )

    @commands.Cog.listener("on_member_join")
    async def account_check(self, member):
        basics = self.bot.get_cog("Basics")

        if member.guild.id == 685870062317600779:
            channel = self.bot.get_channel(685870062317600845)
            guild = self.bot.get_guild(685228508892626971)
            try:
                rb_member = guild.get_member(member.id)
                roles = [role.name for role in rb_member.roles]
                time = member.joined_at - rb_member.joined_at
                await basics.embed(
                    channel,
                    f"{member.display_name} joined RB {abs(int(time.total_seconds() // 86400))} days"
                    f", and {abs(int(time.total_seconds() % 86400 // 3600))} hours ago.\n"
                    f"Roles: {', '.join(roles)}",
                )
            except Exception as e:
                await basics.embed(
                    channel,
                    f"Error: {e}\n"
                    f"{member.display_name} doesn't appear to be a member of RB.",
                )


def setup(bot):
    bot.add_cog(Eevee(bot))
