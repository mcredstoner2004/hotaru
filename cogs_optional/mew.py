import asyncio

from discord import TextChannel

from helpers.mew_helpers import *
from hotaru import cache
from hotaru import data
from helpers import messages


class Mew(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    # ______________Utility________________

    # _____________Listeners_______________

    @commands.Cog.listener()
    async def on_message(self, msg: discord.message):

        """
        this will check all the messages, and manage the xp system
        :param msg: the message that triggered the event
        """
        asyncio.current_task().set_name("mew_on_message")
        if (type(msg.channel) == TextChannel) and (not is_command(self.bot, msg)) and not msg.author.bot:
            guild_id = str(msg.guild.id)
            if (len(msg.content) > 0) and (msg.content[0] == "."):
                await data.ensure_document("support", guild_id)
                support_table = await cache.get_table("support")
                if (key := msg.content[1:]) != "id" and key in support_table[guild_id]:
                    from helpers import messages
                    await messages.Messages("code_block").send_message(msg.channel, support_table[guild_id][key])
                    # await msgs.send_message(msg.channel, support_table[guild_id][key])

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        if payload.guild_id is not None:
            guild_id = str(payload.guild_id)
            await data.ensure_document("roles_on_react", guild_id)
            roles_on_react = (await cache.get_table("roles_on_react"))[guild_id]
            if str(payload.message_id) in roles_on_react:
                message_info = roles_on_react[str(payload.message_id)]
                emoji = payload.emoji
                emoji_id = str(emoji.id if emoji.is_custom_emoji() else emoji.name)
                if emoji_id in message_info:
                    if (guild := self.bot.get_guild(payload.guild_id)) is not None:
                        if (role := guild.get_role(int(message_info[emoji_id]))) is not None:
                            if (member := payload.member) is not None:
                                await member.add_roles(role, reason=f"reacted with {emoji_id}")

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        if payload.guild_id is not None:
            guild_id = str(payload.guild_id)

            await data.ensure_document("roles_on_react", guild_id)
            roles_on_react = (await cache.get_table("roles_on_react"))[guild_id]
            if str(payload.message_id) in roles_on_react:
                message_info = roles_on_react[str(payload.message_id)]
                emoji = payload.emoji
                emoji_id = str(emoji.id if emoji.is_custom_emoji() else emoji.name)
                if emoji_id in message_info:
                    if (guild := self.bot.get_guild(payload.guild_id)) is not None:
                        if (role := guild.get_role(int(message_info[emoji_id]))) is not None:
                            if (member := guild.get_member(payload.user_id)) is not None:
                                await member.remove_roles(role, reason=f"reaction {emoji_id} was removed")

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        if payload.guild_id is not None:
            guild_id = str(payload.guild_id)

            await data.ensure_document("roles_on_react", guild_id)
            roles_on_react = (await cache.get_table("roles_on_react"))[guild_id]
            if str(payload.message_id) in roles_on_react:
                async with data.Connection() as conn:
                    await data.r.table("roles_on_react").get(guild_id).replace(
                        data.r.row.without({str(payload.message_id)})).run(conn)

        # _______________Roles_________________

        #                on xp

    @commands.group()
    @commands.has_guild_permissions(manage_guild=True)
    @commands.guild_only()
    async def roles_on_xp(self, ctx: commands.context):
        """ use this command to configure giving roles to users based on xp
        """
        msgs = messages.Messages()
        if ctx.invoked_subcommand is None:
            await msgs.send_message(ctx.channel, "use ad, remove, or list")

    @roles_on_xp.command(name="list")
    async def roles_on_xp_list(self, ctx: commands.context):

        guild_id = str(ctx.guild.id)

        msgs = messages.Messages()
        # ensure that both the table and the roles on react data exist
        await data.ensure_document("roles_on_xp", guild_id)
        content = ""
        async with data.Connection() as conn:
            (keys := await data.r.table("roles_on_xp").get(guild_id).keys().run(conn)).remove("id")
            for role_id in keys:
                required_xp = await data.r.table("roles_on_xp").get(guild_id)[role_id].run(conn)
                content += f"<@&{role_id}>: {required_xp}\n"
        await msgs.send_message(ctx.channel, content)

    @roles_on_xp.command(name="add")
    async def roles_on_xp_add(self, ctx: commands.context, role: commands.RoleConverter(), required_xp: int):
        guild_id = str(ctx.guild.id)

        msgs = messages.Messages()
        role_id = str(role.id)
        role_existed = False
        async with data.Connection() as conn:
            if await data.r.table("roles_on_xp").get(guild_id).has_fields(role_id).run(conn):
                role_existed = True
            await data.r.table("roles_on_xp").get(guild_id).update({role_id: required_xp}).run(conn)
        if role_existed:
            await msgs.send_message(ctx.channel, "that role already had a required xp, so it was replaced")
        else:
            await msgs.send_message(ctx.channel, "new role on xp added")

    @roles_on_xp.command(name="remove")
    async def roles_on_xp_remove(self, ctx: commands.context, role: commands.RoleConverter()):
        msgs = messages.Messages()
        guild_id = str(ctx.guild.id)
        role_id = str(role.id)
        role_existed = False
        async with data.Connection() as conn:
            if await data.r.table("roles_on_xp").get(guild_id).has_fields(role_id).run(conn):
                role_existed = True
                await data.r.table("roles_on_xp").get(guild_id).replace(data.r.row.without({role_id})).run(conn)
        if role_existed:
            await msgs.send_message(ctx.channel, "now you can't get that role with xp")
        else:
            await msgs.send_message(ctx.channel, "that role didn't have a required xp")

    #              on react

    @commands.group()
    @commands.has_guild_permissions(manage_guild=True)
    @commands.guild_only()
    async def roles_on_react(self, ctx: commands.context):
        """
        use this command to configure giving roles to users when they react to a message with a reaction
        """
        msgs = messages.Messages()
        if ctx.invoked_subcommand is None:
            await msgs.send_message(ctx.channel, "use add, remove, or list")

    @roles_on_react.command(name="list")
    async def roles_on_react_list(self, ctx: commands.context):

        guild_id = str(ctx.guild.id)

        msgs = messages.Messages()
        # ensure that both the table and the roles on react data exist
        await data.ensure_document("roles_on_react", guild_id)
        messages_data = []
        async with data.Connection() as conn:
            (keys := await data.r.table("roles_on_react").get(guild_id).keys().run(conn)).remove("id")
            for message_id in keys:
                messages_data.append(await data.r.table("roles_on_react").get(guild_id)[message_id].run(conn))
        content = ""
        for message_data in messages_data:
            channel_id = message_data["channel_id"]
            (emojis := list(message_data.keys())).remove("channel_id")
            content += f"https://discordapp.com/channels/{ctx.guild.id}/{channel_id}/{message_id}\n"
            for emoji in emojis:
                try:
                    content += f"\t{await ctx.guild.fetch_emoji(int(emoji))}:<@&{message_data[emoji]}>\n"
                except ValueError:
                    content += f"\t{emoji}:<@&{message_data[emoji]}>\n"
        await msgs.send_message(ctx.channel, content)

    @roles_on_react.command(name="add")
    async def roles_on_react_add(self, ctx: commands.context, message: commands.MessageConverter(),
                                 emoji: EmojiConverter(), role: commands.RoleConverter()):
        guild_id = str(ctx.guild.id)

        msgs = messages.Messages()
        message_id = str(message.id)
        emoji_id = str(emoji.id if emoji.is_custom_emoji() else emoji.name)
        role_id = str(role.id)

        try:
            await message.add_reaction(emoji)
            reaction_added = True
        except discord.HTTPException:
            reaction_added = False
            await msgs.send_message(ctx.channel, "adding the reaction failed")
        if reaction_added:
            reaction_existed = False
            async with data.Connection() as conn:
                if await data.r.table("roles_on_react").get(guild_id).has_fields(message_id).run(conn):
                    message_data = await data.r.table("roles_on_react").get(guild_id)[message_id].run(conn)
                    reaction_existed = (emoji_id in message_data)
                await data.r.table("roles_on_react").get(guild_id).update({
                    message_id: {emoji_id: role_id, "channel_id": str(message.channel.id)}}).run(conn)
            if reaction_existed:
                await msgs.send_message(ctx.channel, "that reaction already gave a role, so it was replaced")
            else:

                await msgs.send_message(ctx.channel, "new role on react added")

    @roles_on_react.command(name="remove")
    async def roles_on_react_remove(self, ctx: commands.context, message: commands.MessageConverter(),
                                    emoji: EmojiConverter() = None):
        guild_id = str(ctx.guild.id)

        msgs = messages.Messages()
        message_id = str(message.id)
        if emoji is not None:
            emoji_id = str(emoji.id if emoji.is_custom_emoji() else emoji.name)
            reaction_existed = False
            try:
                await message.clear_reaction(emoji)
                await msgs.send_message(ctx.channel, "that reaction has been removed")
            except discord.HTTPException:
                await msgs.send_message(ctx.channel, "removing the reaction failed")
            async with data.Connection() as conn:
                if await data.r.table("roles_on_react").get(guild_id).has_fields(message_id).run(conn):
                    message_data = await data.r.table("roles_on_react").get(guild_id)[message_id].run(conn)
                    if reaction_existed := (emoji_id in message_data):
                        await data.r.table("roles_on_react").get(guild_id).replace(data.r.row.without({
                            message_id: emoji_id, "channel_id": True
                        })).run(conn)

            if reaction_existed:
                await msgs.send_message(ctx.channel, "that reaction no longer gives a role")
            else:
                await msgs.send_message(ctx.channel, "that reaction didn't give a role")

        else:
            try:
                await message.clear_reactions()
                await msgs.send_message(ctx.channel, "all reactions from that message have been removed")
            except discord.HTTPException:
                await msgs.send_message(ctx.channel, "removing the reactions failed")
            roles_on_react_existed = True
            async with data.Connection() as conn:
                if await data.r.table("roles_on_react").get(guild_id).has_fields(message_id).run(conn):
                    roles_on_react_existed = True
                    await data.r.table("roles_on_react").get(guild_id).replace(data.r.row.without({
                        message_id
                    })).run(conn)
            if roles_on_react_existed:
                await msgs.send_message(ctx.channel, "all of that message's reaction base roles have been removed")
            else:
                await msgs.send_message(ctx.channel, "that message didn't have any roles on reaction")

        # _____________Support_______________

    @commands.group()
    @commands.has_guild_permissions(manage_guild=True)
    @commands.guild_only()
    async def support(self, ctx: commands.context):
        """
        add answers to strings after "."
        user: the user whose xp to modify
        amount: the amount to use to modify the user's xp
        """
        msgs = messages.Messages()
        if ctx.invoked_subcommand is None:
            await msgs.send_message(ctx.channel, "use add, remove, or list")

    @support.command(name="list")
    async def support_list(self, ctx: commands.context):
        guild_id = str(ctx.guild.id)
        msgs = messages.Messages()
        await data.ensure_document("support", guild_id)
        async with data.Connection() as conn:
            keys = await data.r.table("support").get(guild_id).keys().run(conn)
            keys.remove("id")
        await msgs.send_message(ctx.channel, f"there are the following support keys in this server:\n{keys}")

    @support.command(name="add")
    async def support_add(self, ctx: commands.context, key: str, message: str):
        guild_id = str(ctx.guild.id)
        msgs = messages.Messages()
        if key != "id":
            await data.ensure_document("support", guild_id)
            async with data.Connection() as conn:
                key_existed = await data.r.table("support").get(guild_id).has_fields(key).run(conn)
                await data.r.table("support").get(guild_id).update({
                    key: message
                }).run(conn)
            if key_existed:
                await msgs.send_message(ctx.channel, "that support key already exists, so it was replaced")
            else:
                await msgs.send_message(ctx.channel, f"added support key: {key}")
        else:
            await msgs.send_message(ctx.channel, "uhh, I can't let you modify that key")

    @support.command(name="remove")
    async def support_remove(self, ctx: commands.context, key: str):
        guild_id = str(ctx.guild.id)
        msgs = messages.Messages()
        if key != "id":
            await data.ensure_document("support", guild_id)
            async with data.Connection() as conn:
                if key_existed := (await data.r.table("support").get(guild_id).has_fields(key).run(conn)):
                    await data.r.table("support").get(guild_id).replace(data.r.row.without(key)).run(conn)
            if key_existed:
                await msgs.send_message(ctx.channel, f"removed support key: {key}")
            else:
                await msgs.send_message(ctx.channel, "that support key doesn't exist")
        else:
            await msgs.send_message(ctx.channel, "uhh, I can't let you remove that key")

    #  _______________Role Utilities_________________

    @staticmethod
    async def update_roles(member: discord.member, guild: discord.guild):
        """"this function will check the roles_on_xp cache and update an user's roles"""
        await data.ensure_table(f"guild_{guild.id}")
        await data.ensure_document("roles_on_xp", str(guild.id))
        async with data.Connection() as conn:
            user_data = await data.r.table(f"guild_{guild.id}").get(str(member.id)).run(conn)
        user_xp = 0 if (user_data is None) or ("xp" not in user_data) else user_data["xp"]
        keys = list(roles_on_xp := (await cache.get_table("roles_on_xp"))[str(guild.id)])
        keys.remove("id")
        roles_to_add = []
        for role_on_xp in keys:
            if user_xp >= roles_on_xp[role_on_xp]:
                roles_to_add.append(guild.get_role(int(role_on_xp)))
        if len(roles_to_add):
            await member.add_roles(*tuple(roles_to_add), reason="xp based role")


def setup(bot):
    bot.add_cog(Mew(bot))
