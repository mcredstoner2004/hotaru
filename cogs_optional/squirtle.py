"""Functionality exclusive to the Reloaded server."""


from discord.ext import commands
from helpers.checks import allowed_channel, server_manager_or_perms
import asyncio
import discord
import re


url = (
    r"(https?):\/\/"  # protocol
    r"(([\w$\.\+!\*\'\(\),;\?&=-]|%[0-9a-f]{2})+"  # username
    r"(:([\w$\.\+!\*\'\(\),;\?&=-]|%[0-9a-f]{2})+)?"  # password
    r"@)?(?#"  # auth requires @
    r")((([a-z0-9]\.|[a-z0-9][a-z0-9-]*[a-z0-9]\.)*"  # domain segments AND
    r"[a-z][a-z0-9-]*[a-z0-9]"  # top level domain  OR
    r"|((\d|[1-9]\d|1\d{2}|2[0-4][0-9]|25[0-5])\.){3}"
    r"(\d|[1-9]\d|1\d{2}|2[0-4][0-9]|25[0-5])"  # IP address
    r")(:\d+)?"  # port
    r")(((\/+([\w$\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)*"  # path
    r"(\?([\w$\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)"  # query string
    r"?)?)?"  # path and query string optional
    r"(#([\w$\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)?"  # fragment
)


class Squirtle(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener("on_message")
    async def blastoise(self, message):
        """Backup funtionality if Blastoise goes offline."""
        if message.guild.id == 685228508892626971:
            basics = self.bot.get_cog("Basics")
            guild = self.bot.get_guild(685228508892626971)  # Reborn
            blastoise = guild.get_member(771648617110044682)  # Blastoise

            if str(blastoise.status) == "offline":
                if str(message.channel) == "gate" and not message.author.bot:
                    await message.delete()
                    if message.content.lower().startswith(
                        f".gate {message.author.name.lower()}#{message.author.discriminator}=noshop"
                    ):
                        fan = message.author
                        fanclub = guild.get_role(685228525174784033)  # Fan Club

                        print(str(message.author) + ": " + message.content)
                        await fan.add_roles(fanclub)
                        await basics.embed(
                            message.channel,
                            "Is Mr. <@771648617110044682> loafing around, "
                            + "again? I'll give you Fan Club, for now.",
                            delete_after=10
                        )

                    else:
                        await basics.embed(
                            message.channel,
                            "Um, I don't think that was it, did you read the rules?",
                            delete_after=10,
                        )

                elif (
                    message.content.lower().startswith(".")
                    and str(message.channel) == "blastoise-commands"
                ):
                    guild = self.bot.get_guild(685228508892626971)  # Rebranded
                    blastoise = guild.get_member(771648617110044682)  # Blastoise

                    if str(blastoise.status) == "offline":
                        await basics.embed(
                            message.channel,
                            "Mr. <@771648617110044682> is away on, uh, "
                            + '"business". He\'ll be back, probably.',
                        )

    @commands.group()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def salty(self, ctx):
        """The main group for Salty Sunday."""
        basics = self.bot.get_cog("Basics")
        if ctx.invoked_subcommand is None:
            await basics.embed(ctx.channel, "Usually, yeah.")

    @salty.command(aliases=["begin", "open"])
    async def start(self, ctx):
        # TODO: Merge with salty.stop()
        """Open the Salty Sunday channel.

        Hotaru must be able to see the channel to manage it.
        """
        basics = self.bot.get_cog("Basics")
        await (
            ctx.guild.get_channel(726741624603934751).set_permissions(  # Salty Sunday
                ctx.guild.get_role(685228525174784033),  # Fan Club
                read_messages=True,
                reason="Salty Sunday",
            )
        )
        await basics.embed(ctx.channel, "It's time for <#726741624603934751>!")

    @salty.command(aliases=["end", "close"])
    async def stop(self, ctx):
        """Stop Salty Sunday"""
        basics = self.bot.get_cog("Basics")
        await (
            ctx.guild.get_channel(726741624603934751).set_permissions(  # Salty Sunday
                ctx.guild.get_role(685228525174784033),  # Fan Club
                read_messages=None,
                reason="Salty Sunday",
            )
        )
        await basics.embed(ctx.channel, "Alright, everyone chill out.")

    @commands.Cog.listener("on_message")
    async def salty_detection(self, message):
        """A joke saltiness detection script.

        It checks users speaking in the #salty-sunday channel without using
        caps lock, and reposts their messages properly.
        """
        if str(message.channel) == "salty-sunday":
            # These regex substitutions remove emojis, @mentions, and #channels.
            # And now it removes http: URLs, too.
            text = re.sub(r"<a?:[^:]*:\d+>", "", message.content)
            text = re.sub(r"<[@!|#][^>]*\d+>", "", text)
            text = re.sub(url, "", text)

            emotes_count = sum(1 for c in text if c == ":") / 2
            caps_count = sum(1 for c in text if c.isupper())
            total_count = sum(1 for c in text if c.isalpha())
            message_count = len(text)

            # These regex searches are looking for math symbols and Asian enclosed Latin characters.
            math_count = sum(1 for c in text if re.search("[\U0001d400-\U0001d7ff]", c))
            enclosed_count = sum(1 for c in text if re.search("[\u249c-\u24e9]", c))
            extension_count = sum(1 for c in text if re.search("[\u1d43-\u1dbf]", c))
            weird_count = math_count + enclosed_count + extension_count

            print(f"SALTYPOST:{message.author}:{message.content}")

            if total_count != 0 and total_count / message_count < 0.5:
                await message.delete()
            elif total_count != 0 and weird_count / total_count > 0.5:
                await message.delete()
            elif emotes_count > 5:
                await message.delete()
            elif total_count != 0 and (caps_count / total_count) < 1:
                if message.author.id == 502496228731256832:  # mariaa
                    embed = discord.Embed(
                        description="**"
                        + str(message.author).upper()
                        + "**"
                        + ": "
                        + message.content.upper(),
                        color=0x00FF00,
                    )
                else:
                    embed = discord.Embed(
                        description=message.content.upper(), color=0x00FF00
                    )
                    embed.set_author(
                        name=message.author.display_name.upper()
                        + " ("
                        + str(message.author).upper()
                        + ")",
                        icon_url=message.author.avatar_url,
                    )
                await message.delete()
                await message.channel.send(embed=embed)

    @commands.Cog.listener("on_message_edit")
    async def salty_edit(self, before, after):
        """A companion saltiness detection listener for edits."""
        if str(after.channel) == "salty-sunday":
            text = re.sub(r"<a?:[^:]*:\d+>", "", after.content)
            text = re.sub(r"<[@!|#][^>]*\d+>", "", text)
            text = re.sub(url, "", text)

            caps_count = sum(1 for c in text if c.isupper())
            total_count = sum(1 for c in text if c.isalpha())
            message_count = len(text)

            math_count = sum(1 for c in text if re.search("[\U0001d400-\U0001d7ff]", c))
            enclosed_count = sum(1 for c in text if re.search("[\u249c-\u24e9]", c))
            extension_count = sum(1 for c in text if re.search("[\u1d43-\u1dbf]", c))
            weird_count = math_count + enclosed_count + extension_count

            print(f"SALTYEDIT:{after.author}:{after.content}")
            if total_count != 0 and total_count / message_count < 0.5:
                await after.delete()
            elif total_count != 0 and weird_count / total_count > 0.5:
                await after.delete()
            elif total_count != 0 and (caps_count / total_count) < 1:
                embed = discord.Embed(description=after.content.upper(), color=0x00FF00)
                embed.set_author(
                    name=after.author.display_name.upper()
                    + " ("
                    + str(after.author).upper()
                    + ")",
                    icon_url=after.author.avatar_url,
                )
                await after.delete()
                await after.channel.send(embed=embed)

    # @commands.Cog.listener("on_message")
    async def mariaa(self, message):
        """A special script specifically to booli a certain user for spamming caps."""
        if (
            message.author.id == 502496228731256832
            and str(message.channel) != "salty-sunday"
        ):
            text = message.content

            emotes_count = sum(1 for c in text if c == ":") / 2

            text = re.sub(r"<a?:[^:]*:\d+>", "", message.content)
            text = re.sub(r"<[@!|#][^>]*\d+>", "", text)
            text = re.sub(url, "", text)

            caps_ratio = sum(1 for c in text if c.isupper()) / sum(
                1 for c in text if c.isalpha()
            )

            math_count = sum(1 for c in text if re.search("[\U0001d400-\U0001d7ff]", c))
            enclosed_count = sum(1 for c in text if re.search("[\u249c-\u24e9]", c))
            weird_count = math_count + enclosed_count

            if weird_count == True or emotes_count > 5:
                await message.delete()

            elif caps_ratio > 0.3:
                print(f"MARIAAPOST:{message.author}:{message.content}")

                embed = discord.Embed(
                    description="**" + "mariaa" + "**" + ": " + message.content.lower(),
                    color=0x00FF00,
                )
                # // embed = discord.Embed(
                # // description=message.content.lower(),
                # // color=0x00ff00
                # // )
                # // embed.set_author(
                # // name=str(message.author),
                # // icon_url=message.author.avatar_url
                # // )
                await message.delete()
                await message.channel.send(embed=embed)

    # @commands.Cog.listener("on_message_edit")
    async def mariaa_edit(self, before, message):
        if (
            message.author.id == 502496228731256832
            and str(message.channel) != "salty-sunday"
        ):
            text = message.content

            emotes_count = sum(1 for c in text if c == ":") / 2

            text = re.sub(r"<a?:[^:]*:\d+>", "", message.content)
            text = re.sub(r"<[@!|#][^>]*\d+>", "", text)
            text = re.sub(url, "", text)

            caps_ratio = sum(1 for c in text if c.isupper()) / sum(
                1 for c in text if c.isalpha()
            )

            math_count = sum(1 for c in text if re.search("[\U0001d400-\U0001d7ff]", c))
            enclosed_count = sum(1 for c in text if re.search("[\u249c-\u24e9]", c))
            weird_count = math_count + enclosed_count

            if weird_count or emotes_count > 5:
                await message.delete()

            elif caps_ratio > 0.3:
                print(f"MARIAAPOST:{message.author}:{message.content}")

                embed = discord.Embed(
                    description="**" + "mariaa" + "**" + ": " + message.content.lower(),
                    color=0x00FF00,
                )
                # embed = discord.Embed(
                #     description=message.content.lower(),
                #     color=0x00ff00
                # )
                #  embed.set_author(
                #     name=str(message.author),
                #     icon_url=message.author.avatar_url
                # )
                await message.delete()
                await message.channel.send(embed=embed)


def setup(bot):
    bot.add_cog(Squirtle(bot))
