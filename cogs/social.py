"""The social cog governs general user interactions."""

from discord.ext import commands
from helpers.checks import allowed_channel, server_manager_or_perms
import datetime
import discord
import inflect
import math
import random
from hotaru import data


class Social(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def audit(self, ctx, user: int = None, length: int = None):
        """Returns a user's farm history to check for suspected botting.

        Unlike most other commands, the user arg here is an ID to allow for checking
        users outside the current guild or channel.

        Args:
            ctx ([type]): [description]
            user (int, optional): The user's ID. Defaults to None.
            length (int, optional): The number of entries to return. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")

        if user is None:
            user = ctx.author.id
        async with data.Connection() as conn:
            history = await data.r.table("user_data").get(user)["farm_history"].run(conn)
        seconds = []

        if length is not None:
            length = int(length)
            for entry in history[-length:]:
                seconds.append(str(entry[1]))

        else:

            for entry in history:
                seconds.append("{:012.3f}".format(entry[1]))

        joined = "\n".join(seconds)

        await basics.embed(ctx.channel, f"```\n{joined}```")

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def hyperfarm(self, ctx):
        """An administrative farm for testing.

        Ignores the last farm check.
        """
        basics = self.bot.get_cog("Basics")
        bank = self.bot.get_cog("Bank")
        async with data.Connection() as conn:
            try:
                last_farm = (
                    await data.r.table("user_data")
                        .get(ctx.author.id)["last_farm"]
                        .run(conn)
                )
                delta_farm = (
                        ctx.message.created_at.astimezone(data.r.make_timezone("00:00"))
                        - last_farm
                )

            except:  # TODO make except more specific
                last_farm = None
                delta_farm = None

            await bank.deposit(ctx.author, 25, "red")
            await bank.deposit(ctx.author, 25, "green")
            this_farm = data.r.expr(
                ctx.message.created_at.astimezone(data.r.make_timezone("00:00"))
            )
            await data.r.table("user_data").get(ctx.author.id).update(
                {"last_farm": this_farm}
            ).run(conn)

            keys = await data.r.table("user_data").get(ctx.author.id).keys().run(conn)

            if "farm_history" in keys:
                if (
                        await data.r.table("user_data")
                                .get(ctx.author.id)["farm_history"]
                                .run(conn)
                        is not None
                ):
                    await data.r.table("user_data").get(ctx.author.id).update(
                        {
                            "farm_history": data.r.row["farm_history"].append(
                                (this_farm, delta_farm.total_seconds())
                            )
                        }
                    ).run(conn)
            else:
                await data.r.table("user_data").get(ctx.author.id).update(
                    {"farm_history": [(this_farm, 0)]}
                ).run(conn)

            await basics.embed(
                ctx.channel,
                f"{ctx.author.display_name} farmed 25 coins and 25 tokens.\n"
                + f"Coins are for keeping and tokens are for giving.",
            )

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def farm(self, ctx):
        """Collect currency hourly.

        Checks the database for the most recent successful farm and updates
        currency and history entries after.
        """
        # ? This function may currently be exploitable. Running the command in
        # ? parallel can allow multiple farms to initiate before the 'last_farm'
        # ? entry is updated. Currently only happens to bots when Hotaru lags.
        basics = self.bot.get_cog("Basics")
        bank = self.bot.get_cog("Bank")
        p = inflect.engine()
        async with data.Connection() as conn:
            if ctx.guild.name == "Rebirth" and ctx.channel.name != "blastoise-commands":
                await basics.embed(
                    ctx.channel,
                    "Uh... Mr. Blastoise said we have to farm in <#685228542698848342>.",
                )
                return

            await bank.check_user(ctx.author)
            try:
                last_farm = (
                    await data.r.table("user_data")
                        .get(ctx.author.id)["last_farm"]
                        .run(conn)
                )
                delta_farm = (
                        ctx.message.created_at.astimezone(data.r.make_timezone("00:00"))
                        - last_farm
                )

            except:  # TODO make except more specific
                last_farm = None
                delta_farm = None

            if last_farm is not None:
                try:
                    # botting detection
                    # history = await data.r.table('user_data').get(ctx.author.id)['farm_history'].run(conn)
                    pass
                except:  # TODO make except more specific
                    pass

            if last_farm is None or delta_farm > datetime.timedelta(hours=1):
                await bank.deposit(ctx.author, 25, "red")
                await bank.deposit(ctx.author, 25, "green")
                this_farm = data.r.expr(
                    ctx.message.created_at.astimezone(data.r.make_timezone("00:00"))
                )
                await data.r.table("user_data").get(ctx.author.id).update(
                    {"last_farm": this_farm}
                ).run(conn)

                keys = await data.r.table("user_data").get(ctx.author.id).keys().run(conn)

                if "farm_history" in keys:
                    if (
                            await data.r.table("user_data")
                                    .get(ctx.author.id)["farm_history"]
                                    .run(conn)
                            is not None
                    ):
                        await data.r.table("user_data").get(ctx.author.id).update(
                            {
                                "farm_history": data.r.row["farm_history"].append(
                                    (this_farm, delta_farm.total_seconds())
                                )
                            }
                        ).run(conn)
                else:
                    await data.r.table("user_data").get(ctx.author.id).update(
                        {"farm_history": [(this_farm, 0)]}
                    ).run(conn)

                await basics.embed(
                    ctx.channel,
                    f"{ctx.author.display_name} farmed 25 coins and 25 tokens.\n"
                    + f"Coins are for keeping and tokens are for giving.",
                )

            else:
                time_left = datetime.timedelta(hours=1) - delta_farm
                minutes_left = math.ceil(time_left / datetime.timedelta(minutes=1))
                await basics.embed(
                    ctx.channel,
                    f"It takes an hour for a new crop to be ready!\n"
                    + f'You still have to wait {minutes_left} {p.plural("minute", minutes_left)}.',
                )

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def give(self, ctx, user: discord.Member = None, amt: int = 0):
        """Give some coins to another user.

        The give command will always give your tokens first, converting them to
        coins. If that isn't enough, the difference is made up with regular coins.

        Args:
            ctx ([type]): [description]
            user (discord.Member, optional): The target of the gift. Defaults to None.
            amt (int, optional): The amount to give. Defaults to 0.
        """
        basics = self.bot.get_cog("Basics")
        bank = self.bot.get_cog("Bank")
        p = inflect.engine()

        if amt <= 0:
            await basics.embed(ctx.channel, "Here's your cardboard sign.")
            return

        if amt > 1000000:
            await basics.embed(ctx.channel, "... yeah right, moneybags.")
            return

        if user is None:
            prefix = self.bot.command_prefix
            await basics.embed(
                ctx.channel,
                f"`{prefix}give <user> <amount>`.\n"
                + f"Tokens are useless, but become coins "
                + f"when you `{prefix}give` them away.\n"
                + f"`{prefix}give` always gives tokens before coins.",
            )

        if user is not None and user.id != ctx.author.id:
            await bank.check_user(ctx.author)
            await bank.check_user(user)
            async with data.Connection() as conn:
                balance = (
                    await data.r.table("user_data")
                        .get(ctx.author.id)
                        .pluck(["red", "green", "blue"])
                        .values()
                        .run(conn)
                )
            red = balance[2]
            green = balance[1]

            if amt <= green:
                await bank.withdraw(ctx.author, amt, "green")
            elif amt <= (green + red):
                await bank.withdraw(ctx.author, green, "green")
                await bank.withdraw(ctx.author, amt - green, "red")
            elif amt > (green + red):
                coins_short = amt - green - red
                await basics.embed(
                    ctx.channel,
                    f"You don't have enough for that. "
                    + f"You're {coins_short} {p.plural('coin', coins_short)} short.",
                )
                return

            await bank.deposit(user, amt, "red")
            await basics.embed(
                ctx.channel,
                f"{ctx.author.display_name} gave "
                + f"{user.display_name} {amt} {p.plural('coin', amt)}.",
            )
        elif ctx.author.id == user.id:
            await basics.embed(ctx.channel, "This one's a capitalist.")

    @give.error
    async def give_error(self, ctx, e):
        basics = self.bot.get_cog("Basics")
        print(e)
        if isinstance(e, (commands.ConversionError, commands.BadArgument)):
            prefix = self.bot.command_prefix
            await basics.embed(
                ctx.channel,
                f"`{prefix}give <user> <amount>`.\n"
                + f"Tokens are useless, but become coins "
                + f"when you `{prefix}give` them away.\n"
                + f"`{prefix}give` always gives tokens before coins.",
            )

    @commands.command(aliases=["table", "tableflip"])
    @commands.guild_only()
    async def flip(self, ctx):
        """Produce a randomly-generated table flip."""
        basics = self.bot.get_cog("Basics")
        flips = [
            r"(╯°Д°)╯︵/(.□ . \)",
            r"(˚Õ˚)ر ~~~~╚╩╩╝",
            r"ヽ(ຈل͜ຈ)ﾉ︵ ┻━┻",
            r"(ノಠ益ಠ)ノ彡┻━┻",
            r"(╯°□°)╯︵ ┻━┻",
            r"(┛◉Д◉)┛彡┻━┻",
            r"┻━┻︵ \(°□°)/ ︵ ┻━┻",
            r"(┛ಠ_ಠ)┛彡┻━┻",
        ]
        await basics.embed(ctx.channel, random.choice(flips))

    @commands.command(aliases=["flop", "putback"])
    @commands.guild_only()
    async def unflip(self, ctx):
        """Produce a randomly-generated table un-flip"""
        basics = self.bot.get_cog("Basics")
        unflips = [r"┳━┳ ヽ(ಠل͜ಠ)ﾉ", r"┬─┬ノ( º _ ºノ)", r"┏━┓┏━┓┏━┓ ︵ /(^.^/)"]
        await basics.embed(ctx.channel, random.choice(unflips))


def setup(bot):
    bot.add_cog(Social(bot))
