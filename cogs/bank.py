"""Functionality for manipulating users' currency."""

from discord.ext import commands
from helpers.checks import allowed_channel, server_manager_or_perms
import asyncio
import discord
import inflect
from hotaru import data


class Bank(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def check_user(self, user):
        user_id = user.id
        user_name = user.name + "#" + user.discriminator
        user_nick = user.display_name
        async with data.Connection() as conn:
            if await data.r.table("user_data").get(user_id).run(conn) is None:
                await data.r.table("user_data").insert(
                    {
                        "id": user_id,
                        "name": user_name,
                        "nick": user_nick,
                        "red": 0,
                        "green": 0,
                        "blue": 0,
                    }
                ).run(conn)
                print(f"Added user {user_name} to `user_data`.")

            else:
                await data.r.table("user_data").get(user_id).update(
                    {"name": user_name, "nick": user_nick}
                ).run(conn)

    async def deposit(self, user, amt: int = 0, currency: str = None):
        """Deposit currency into the users' account.

        Args:
            user (discord.User): The target of the deposit.
            amt (int, optional): The amount to deposit. Defaults to 0.
            currency (str, optional): The type of currency. Defaults to None.
        """
        currency_types = ["red", "green", "blue"]
        currency = currency.lower()

        if amt < 0:
            print("ERROR: deposit amount less than 0")

        else:
            async with data.Connection() as conn:
                if user is not None and currency in currency_types:
                    await self.check_user(user)
                    await data.r.table("user_data").get(user.id).update(
                        {currency: (data.r.row[currency].default(0) + amt)}
                    ).run(conn)

                balance = await data.r.table("user_data").get(user.id)[currency].run(conn)

            print(
                f"{user.name}#{user.discriminator}: "
                + f"+{amt}{currency} ={balance}{currency}"
            )

    async def withdraw(self, user, amt: int = 0, currency: str = None):
        """Withdraw currency from the user's account.

        Will fail if it causes the amount to fall below zero.

        Args:
            user (discord.User): The target of the withdrawal.
            amt (int, optional): The amount to withdrawal. Defaults to 0.
            currency (str, optional): The type of currency. Defaults to None.
        """

        currency_types = ["red", "green", "blue"]
        currency = currency.lower()

        if amt < 0:
            print("ERROR: withdrawal amount less than 0")

        else:
            if user is not None and currency in currency_types:
                await self.check_user(user)
                async with data.Connection() as conn:
                    cash = await data.r.table("user_data").get(user.id)[currency].run(conn)

                    if cash is not None and (cash - amt >= 0):
                        await data.r.table("user_data").get(user.id).update(
                            {currency: (data.r.row[currency].default(0) - amt)}
                        ).run(conn)
                        balance = (
                            await data.r.table("user_data").get(user.id)[currency].run(conn)
                        )
                        print(
                            f"{user.name}#{user.discriminator}: "
                            + f"-{amt}{currency} ={balance}{currency}"
                        )

                    else:
                        print(f"ERROR: {user.name}#{user.discriminator} lacks the funds")

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def balance(self, ctx, user: discord.Member = None):
        """Check a user's account balances.

        As ESP functionality is server-exclusive, this function currently doesn't
        display the value for blue coins.

        Args:
            user (discord.Member, optional): [description]. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        p = inflect.engine()

        if user is None:
            user = ctx.author

        await self.check_user(user)
        async with data.Connection() as conn:
            balance = (
                await data.r.table("user_data")
                    .get(user.id)
                    .pluck(["red", "green", "blue"])
                    .values()
                    .run(conn)
            )
        prefix = self.bot.command_prefix
        await basics.embed(
            ctx.channel,
            f'{user.display_name} has {balance[2]} {p.plural("coin", balance[2])} '
            + f'and {balance[1]} {p.plural("token", balance[1])}.\n'
            + f"Tokens are useless, but become coins "
            + f"when you `{prefix}give` them away.\n"
            + f"`{prefix}give` always gives tokens before coins.",
        )
        print(
            f"{user.name}#{user.discriminator}: "
            + f"{balance[2]}red {balance[1]}green {balance[0]}blue"
        )

    @commands.command(aliases=["stamp"])
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def stamps(self, ctx, user: discord.Member = None):
        """Check a user's number of stamps.

        This function is server exclusive and doesn't display coins or tokens.

        Args:
            user (discord.Member, optional): [description]. Defaults to None.
        """
        p = inflect.engine()

        await ctx.message.delete()

        if user is None:
            user = ctx.author

        await self.check_user(user)
        async with data.Connection() as conn:
            balance = (
                await data.r.table("user_data")
                    .get(user.id)
                    .pluck(["red", "green", "blue"])
                    .values()
                    .run(conn)
            )
        embed = discord.Embed(
            color=0x00FF00,
            description=(
                f"{user.display_name} has {balance[0]} {p.plural('stamp', balance[0])}."
            ),
        )
        await ctx.send(f"<@{ctx.author.id}>", embed=embed, delete_after=10)
        print(
            f"{user.name}#{user.discriminator}: "
            + f"{balance[2]}red {balance[1]}green {balance[0]}blue"
        )

    @stamps.error
    async def stamps_error(self, ctx, e):
        # TODO: Move to decorator.
        if isinstance(e, commands.BadArgument):
            basics = self.bot.get_cog("Basics")
            await ctx.message.delete()
            embed = discord.Embed(
                color=0x00FF00, description="I'm not sure who that is."
            )
            await ctx.send(f"<@{str(ctx.author.id)}>\n", embed=embed, delete_after=10)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def set(
            self, ctx, user: discord.Member = None, amt: int = 0, currency: str = None
    ):
        """Administrative task to set currency.

        Args:
            user (discord.Member, optional): The user account to use. Defaults to None.
            amt (int, optional): The amount to set to. Defaults to 0.
            currency (str, optional): The currency to set. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")

        if user is not None:
            await self.check_user(user)
            async with data.Connection() as conn:
                await data.r.table("user_data").get(user.id).update(
                    {
                        currency: amt,
                    }
                ).run(conn)
            await basics.embed(
                ctx.channel, f"{user.display_name}'s {currency} coins set to {amt}."
            )
            print(f"{user.name}#{user.discriminator}: set {amt}{currency}")

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def reset(self, ctx, user: discord.Member = None):
        """Reset a user's account; exclusively used for testing.

        Args:
            user (discord.Member, optional): The user to reset. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")

        if user is not None:
            await self.check_user(user)
            async with data.Connection() as conn:
                await data.r.table("user_data").get(user.id).update(
                    {
                        "red": 0,
                        "green": 0,
                        "blue": 0,
                        "last_farm": None,
                        "farm_history": None,
                        "last_peel": None,
                        "peel_history": None,
                    }
                ).run(conn)
            await basics.embed(
                ctx.channel, f"{user.display_name}'s bank account was reset."
            )
            print(f"{user.name}#{user.discriminator}: reset")


def setup(bot):
    bot.add_cog(Bank(bot))
