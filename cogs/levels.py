"""The Levels cog covers functionality related to experience, levelling, and xp leaderboards."""
import math
from collections import namedtuple

import asyncio

from helpers.mew_helpers import *
from hotaru import data
from helpers import settings, messages

global_table = "user_data"
page_size = 10


def get_level(xp: int) -> int:
    return int(1 / 6 * (((10 * math.sqrt(81 * (xp ** 2) - 39375 * xp + 5312501) + 90 * xp - 21875) ** (1 / 3)) - (
            75 * (5 ** (2 / 3))) / ((2 * math.sqrt(81 * (xp ** 2) - 39375 * xp + 5312500) + 18 * xp - 4375) ** (1 / 3))
                        + 25))


def get_required_xp(lvl: int) -> int:
    return math.ceil(6 * lvl ** 3 / 5 - 15 * lvl ** 2 + 100 * lvl)


class Levels(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        settings.BoolSetting("guild_xp", default_value=True, success_msg="guild xp set to {value}")
        settings.FloatSetting("base_xp", default_value=10, min_val=0, success_msg="ok, base xp set to {value}",
                              range_err_msg="dont you want people to earn experience?")
        settings.FloatSetting("xp_decay", default_value=0.95, min_val=0, lopen=False,
                              max_val=1, ropen=False, success_msg="alright, xp decay is now {value}",
                              range_err_msg="trust me, you don't want to see what happens when xp decay is "
                                            + "not between 0 and 1")
        settings.IntSetting("min_message_xp", min_val=0, default_value=10,
                            success_msg="now people will always get at least {value} xp per message",
                            range_err_msg="why would you want negative minimum xp? 0 should do",
                            val_err_msg="yeah.. this has to be an integer")
        settings.IntSetting("max_decay_level", min_val=0, default_value=60,
                            success_msg="done, max decay level is now {value}",
                            range_err_msg="uhh, I was told to keep this greater than 0 for... reasons",
                            val_err_msg="yeah.. this has to be an integer")
        settings.FloatSetting("decay_duration", min_val=0, lopen=True, default_value=150,
                              success_msg="from now on, the xp decay will last for {value} seconds",
                              range_err_msg="I set this to a negative number once... don't try it... nor 0",
                              val_err_msg="fun fact: decay duration must be a number")
        settings.IntSetting("min_xp_message_length", min_val=0, default_value=4,
                            success_msg="now messages need to be at least {value} characters long to give any xp",
                            range_err_msg="setting this to a negative makes no sense, just use 0",
                            val_err_msg="yeah.. this has to be an integer")
        settings.IntSetting("min_xp_message_words", min_val=0, default_value=2,
                            success_msg="messages will now only give xp if they have more than {value} words",
                            range_err_msg="you can't have negative words, so don't give me a negative number",
                            val_err_msg="yeah.. this has to be an integer")
        settings.IntSetting("max_xp_message_length", min_val=0, default_value=200,
                            success_msg="now messages longer than {value} characters will  count as having {value}",
                            range_err_msg="there's no such thing as negative characters",
                            val_err_msg="yeah.. this has to be an integer")
        settings.ChannelListSetting("disabled_channels", add_action="disable", remove_action="enable",
                                    add_success_msg="xp for channel {channel} is now disabled",
                                    add_err_msg="xp for channel {channel} was already disabled",
                                    remove_success_msg="xp for channel {channel} is now enabled",
                                    remove_err_msg="xp for channel {channel} was already enabled"
                                    )
        settings.UserListSetting("xp_banned_users", add_action="ban", remove_action="unban",
                                 add_success_msg="user {user} is now banned",
                                 add_err_msg="user {user} was already banned",
                                 remove_success_msg="user {user} is no longer banned",
                                 remove_err_msg="user {user} wasn't banned"
                                 )

    # _____________Experience_______________

    #              Listeners

    @commands.Cog.listener()
    async def on_message(self, msg: discord.message):

        """
        this will check all the messages, and manage the xp system
        :param msg: the message that triggered the event
        """
        asyncio.current_task().set_name("levels_on_message")
        if (type(msg.channel) == discord.TextChannel) and (not is_command(self.bot, msg)) and not msg.author.bot:
            spam = await is_spam(msg.content, guild=msg.channel.guild)
            guild_id = str(msg.guild.id)
            await self.update_xp_data(msg, guild_id, spam)
            await self.update_xp_data(msg, None, spam)

    #              checking

    @commands.group(aliases=["top"])
    @commands.guild_only()
    async def leaderboard(self, ctx: commands.context, *args):
        """Shows this guild"s leaderboard, you can add a number to view that page of the leaderboard
        :rtype: str
        :param ctx: the context in which the command was invoked
        """
        page = None
        first_wrong_arg = None
        for arg in args:
            try:
                page = int(arg)
            except ValueError:
                if (arg != "global") and first_wrong_arg is None:
                    first_wrong_arg = arg
        if page is None:
            page = 1
        if "global" in args:
            # generates the global leaderboard page
            embed = await self.generate_leaderboard_page(global_table, page, None)
            embed.set_thumbnail(url=self.bot.user.avatar_url)
            guild = None
            show_rank = True
        else:
            # generates the leaderboard page
            embed = await self.generate_leaderboard_page(f"guild_{ctx.guild.id}", page, ctx.guild)
            embed.set_thumbnail(url=ctx.guild.icon_url)
            guild = ctx.guild
            if first_wrong_arg is not None:
                embed.add_field(name=f"I don't know what to do with {first_wrong_arg} ",
                                value="so this is this guild's leaderboard")
                show_rank = False
            else:
                show_rank = True

        # gets the rank and xp of the user that sent the command
        if show_rank:
            user_rank = await get_user_rank(ctx.author, guild)
            face = "😐"
            if user_rank[0] < 4:
                face = "\ud83d\ude42"
                if user_rank[0] == 1:
                    face = "😀"
            embed.add_field(name="Your rank and XP", value=f"{face}  |  rank: {user_rank[0]} | XP: {user_rank[1]}")
        if "delete_after" in settings.settings_list:
            delete_after = await settings.settings_list["delete_after"].get_value(str(ctx.guild.id))
        else:
            delete_after = 30
        await ctx.send(embed=embed, delete_after=(delete_after if delete_after != 0 else None))
        try:
            await ctx.message.delete()
        except(discord.NotFound, discord.Forbidden):
            pass

    async def generate_leaderboard_page(self, table_name: str, page: int, guild: discord.Guild = None) -> discord.Embed:
        UserEntry = namedtuple("UserEntry", "id xp")
        user_entries = []
        await data.ensure_table(table_name)
        await data.ensure_table_index(table_name, "xp")
        async with data.Connection() as conn:
            # count the total amount of users, to limit the pages
            total_users = await data.r.table(table_name).between(1, data.r.maxval, index="xp").count().run(conn)
            # limit the page to the range 1-math.ceil(total_users/10)
            page = max(min(math.ceil(total_users / page_size), page), 1)
            n = page * page_size - page_size
            async for user in await data.r.table(table_name).between(1, data.r.maxval, index="xp").order_by(
                    index=data.r.desc("xp")).slice(page_size * page - page_size,
                                                   page_size * page).run(conn):  # get the users
                user_entries.append(UserEntry(int(user["id"]), user["xp"]))

        if guild is not None:  # set the page"s title to the corresponding string
            embed = discord.Embed(title=f"{guild.name} | page: {page}", color=0x00ff00)
            for user_entry in user_entries:
                n = n + 1
                discord_member = guild.get_member(user_entry.id)
                user_name = discord_member.display_name if discord_member is not None else str(user_entry.id)
                embed.add_field(name=f"{n}.\t{user_name}",
                                value=f"XP: {user_entry.xp}\tLVL: {get_level(user_entry.xp)}", inline=False)
                # get the nickname if available for the guild leaderboard
        else:
            embed = discord.Embed(title=f"global | page: {page}", color=0x00ff00)
            for user_entry in user_entries:
                n = n + 1
                discord_user = self.bot.get_user(user_entry.id)
                user_name = discord_user.name if discord_user is not None else str(user_entry.id)
                embed.add_field(name=f"{n}.\t{user_name}",
                                value=f"XP: {user_entry.xp}\tLVL: {get_level(user_entry.xp)}", inline=False)
                # get the user name if available for the global leaderboard

        return embed

    @commands.group(aliases=["points", "level"])
    async def rank(self, ctx: commands.context, user: MemberUserConverter() = None):
        """Shows your points, or you can specify an user to view theirs"
        :param user:
        :rtype: str
        :param ctx: the context in which the command was invoked
        """
        if user is None:
            user = ctx.author
        if user.id != 0:
            if ctx.author == user:
                user_string = "your"
            else:
                if user.display_name[-1] == "s":
                    user_string = user.display_name + "'"
                else:
                    user_string = user.display_name + "'s"
            if ctx.subcommand_passed == "global":
                guild = None
                guild_string = "global"
                invalid_subcommand = False
            else:
                guild = ctx.guild
                guild_string = ""
                invalid_subcommand = False
                if ctx.subcommand_passed is not None:
                    invalid_subcommand = True
            user_rank = await get_user_rank(user, guild)
            embed = discord.Embed(title=f"{user_string} {guild_string} rank", color=0x00ff00,
                                  description=f"rank: {user_rank[0]} | XP: {user_rank[1]} |" +
                                              f" LV:{get_level(user_rank[1])}")
            if invalid_subcommand:
                embed.add_field(name="you can only check this guild's or the global rank",
                                value=f"so this is this {user_string} rank in this guild")
            if "delete_after" in settings.settings_list:
                delete_after = await settings.settings_list["delete_after"].get_value(str(ctx.guild.id))
            else:
                delete_after = 30
            await ctx.send(embed=embed, delete_after=delete_after)
            try:
                await ctx.message.delete()
            except(discord.NotFound, discord.Forbidden):
                pass

    #              managing

    @commands.group()
    @commands.has_guild_permissions(manage_guild=True)
    @commands.guild_only()
    async def manage_points(self, ctx: commands.context, action: str, user: MemberUserConverter(), amount: int):
        mew = None
        """
        action: set|add|give|remove|subtract
        user: the user whose xp to modify
        amount: the amount to use to modify the user's xp
        """
        table_name = f"guild_{ctx.guild.id}"
        user_id = str(user.id)
        if user.display_name[-1] == "s":
            belongs_string = "'"
        else:
            belongs_string = "'s"

        msgs = messages.Messages()
        # ensure that both the table and the user"s data exist
        await data.ensure_table(table_name)
        await data.ensure_table_index(table_name, "xp")
        if user.id != 0:
            await ensure_user_data(table_name, user_id)
            if action == "set":  # if the amount is not negative set the user"s xp to that amount
                if amount >= 0:
                    async with data.Connection() as conn:
                        await data.r.table(table_name).get(user_id).update({
                            "xp": amount
                        }).run(conn)
                    await msgs.send_message(ctx.channel, f"set {user.display_name + belongs_string} xp to  {amount}")
                    if (type(user) == discord.member.Member) and ((mew := self.bot.get_cog("Mew")) is not None):
                        await mew.update_roles(user, ctx.guild)
                else:
                    await msgs.send_message(ctx.channel, "you can't set an user's xp to a negative number")
            elif action in ["give", "add", "remove", "subtract"]:  # if the final xp is not negative set it xp
                async with data.Connection() as conn:
                    user_data = await data.r.table(table_name).get(user_id).run(conn)
                user_xp = 0 if (user_data is None) or ("xp" not in user_data) else user_data["xp"]
                amount = amount if action in ["give", "add"] else -amount
                if user_xp + amount >= 0:
                    async with data.Connection() as conn:
                        await data.r.table(table_name).get(user_id).update({"xp": user_xp + amount}).run(conn)
                    await msgs.send_message(ctx.channel,
                                            f"set {user.display_name + belongs_string} xp to  {user_xp + amount}")
                    if type(user) == discord.member.Member and ((mew := self.bot.get_cog("Mew")) is not None):
                        await mew.update_roles(user, ctx.guild)
                else:
                    async with data.Connection() as conn:
                        await data.r.table(table_name).get(user_id).update({
                            "xp": 0
                        }).run(conn)
                    await msgs.send_message(ctx.channel, f"set {user.display_name + belongs_string} xp to  {0}")
        else:
            await msgs.send_message(ctx.channel, "user not found")

    async def update_xp_data(self, msg: discord.message, guild_id, spam: bool = False):
        """
        calculates the xp to be gained and updates that user"s data in the database
        :param guild_id: the guild id of the table to modify
        :param msg: the message which points are to be calculated
        :param spam: a bool indicating if the message is spam
        """

        author = msg.author
        author_id = str(author.id)
        table = global_table if guild_id is None else f"guild_{guild_id}"
        guild_id = "global" if guild_id is None else guild_id

        message_time = msg.created_at.astimezone(data.r.make_timezone("00:00"))
        #  ensures that the guild"s table exists
        await data.ensure_table(table)
        await data.ensure_table_index(table, "xp")

        if await settings.settings_list["guild_xp"].get_value(guild_id):  # checks if guild xp is enabled
            decay_duration = await settings.settings_list["decay_duration"].get_value(guild_id)
            max_decay_level = await settings.settings_list["max_decay_level"].get_value(guild_id)
            await ensure_user_data(table, author_id)
            async with data.Connection() as conn:
                user_data = await data.r.table(table).get(author_id).run(conn)
                #  reads data from the user
            user_xp = user_data["xp"] if "xp" in user_data else 0
            last_message_time = user_data["last_message_time"] if "last_message_time" in user_data else message_time
            decay_level = user_data["decay_level"] if "decay_level" in user_data else 0
            recovered_time = user_data["recovered_time"] if "recovered_time" in user_data else 0
            #  calculates the decay level, the xp, and updates the database
            delta_seconds = (message_time - last_message_time).total_seconds() + recovered_time
            decay_level = min(max(0, decay_level - math.floor(delta_seconds / decay_duration)), max_decay_level)
            message_xp = calculate_message_value(msg, spam=spam, g_settings=await settings.get_all_settings(guild_id),
                                                 decay_level=decay_level)
            async with data.Connection() as conn:
                await data.r.table(table).get(author_id).update({
                    "xp": user_xp + message_xp,
                    "last_message_time": message_time,
                    "recovered time": delta_seconds % decay_duration,
                    "decay_level": decay_level + 1,
                }).run(conn)
            if (type(msg.author) == discord.member.Member) and (guild_id != "global") and str(
                    msg.author.id) not in await settings.settings_list["xp_banned_users"].get_value(guild_id):
                if (mew := self.bot.get_cog("Mew")) is not None:
                    await mew.update_roles(msg.author, msg.guild)


#  _______________XP Utilities_________________


def calculate_message_value(msg: discord.message, spam: bool = False, g_settings: dict = None,
                            decay_level: int = 0) -> int:
    """

    :rtype: int
    :param msg: the message to calculate the value for
    :param spam: a bool indicating if the message was detected as spam
    :param g_settings: the xp related settings
    :param decay_level: the decay level the user is at
    :return: the xp this message should award
    """
    if spam:
        return 0
    elif g_settings is not None:
        #  loads the settings
        base_xp = g_settings["base_xp"] if "base_xp" in g_settings else 10
        xp_decay = g_settings["xp_decay"] if "xp_decay" in g_settings else 0.95
        min_message_xp = g_settings["min_message_xp"] if "min_message_xp" in g_settings else 1
        min_xp_message_length = g_settings["min_xp_message_length"] if "min_xp_message_length" in g_settings else 4
        min_xp_message_words = g_settings["min_xp_message_words"] if "min_xp_message_words" in g_settings else 2
        max_xp_message_length = g_settings["max_xp_message_length"] if "max_xp_message_length" in g_settings else 200
        disabled_channels = g_settings["disabled_channels"] if "disabled_channels" in g_settings else []
        xp_banned_users = g_settings["xp_banned_users"] if "xp_banned_users" in g_settings else []
    else:
        #  default settings, in case None was passed
        base_xp = 10
        xp_decay = 0.95
        min_message_xp = 1
        min_xp_message_length = 4
        min_xp_message_words = 2
        max_xp_message_length = 200
        disabled_channels = []
        xp_banned_users = []
    #  removes emoji, counts the words and the characters, if it passes the check, returns xp according to
    #  the log base 4 of the length, this might be changed for another formula
    emojiless_text, total_emoji = remove_custom_emojis(msg.content)
    linkless_text, total_links = remove_links(emojiless_text)
    word_count = count_words(linkless_text)
    total_characters = min(len(remove_whitespace(linkless_text)), max_xp_message_length)
    if (total_characters >= min_xp_message_length) and (word_count >= min_xp_message_words) and (str(
            msg.channel.id) not in disabled_channels) and (str(msg.author.id) not in xp_banned_users):
        multiplier = math.log(total_characters, 4)
        return math.floor(max(base_xp * multiplier * xp_decay ** decay_level, min_message_xp))
    else:
        return 0


async def get_user_rank(user: discord.user, guild: discord.guild = None) -> (int, int):
    """
    method that checks a user"s rank and xp,
    :rtype: (int,int)
    :param user: the user to check the rank of
    :param guild: the guild to check the rank in, or None checks the global rank
    :return: a tuple containing the rank and the xp
    """
    table = global_table if guild is None else f"guild_{guild.id}"
    user_id = str(user.id)
    await data.ensure_table(table)
    await data.ensure_table_index(table, "xp")
    await ensure_user_data(table, user_id)
    async with data.Connection() as conn:
        user_data = await data.r.table(table).get(user_id).run(conn)  # gets the user data
        return await data.r.table(table).between(user_data["xp"], data.r.maxval, index="xp").count().run(
            conn), user_data["xp"]


#  _____________Database Utilities_______________

async def ensure_user_data(table_name: str, user_id: str):
    async with data.Connection() as conn:
        await data.r.table(table_name).get(user_id).do(
            lambda user_data: data.r.branch(user_data, {}, data.r.table(table_name).insert({
                "id": user_id,
                "xp": 0,
                "last_message_time": data.r.now().in_timezone("+00:00"),
                "recovered time": 0,
                "decay_level": 0
            }))
        ).run(conn)


def setup(bot):
    bot.add_cog(Levels(bot))
