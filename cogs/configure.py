"""Guild configuration and permissions management."""

from discord.ext import commands
from helpers.checks import server_manager_or_perms
from helpers.objects import Emotes, Permissions
from helpers import messages, settings
from hotaru import data
import discord
import json
import os


class Configure(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(aliases=["configure"])
    @commands.guild_only()
    @server_manager_or_perms()
    async def config(self, ctx):
        """The main config group."""
        basics = self.bot.get_cog("Basics")
        if ctx.invoked_subcommand is None:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")

    @config.command()
    async def list_commands(self, ctx, cog: str = None):
        """List the available commands for this bot.

        If a cog is specified, only list the commands for that cog.

        Args:
            cog (str, optional): The cog to list commands for. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        if cog is not None and cog.title() in ctx.bot.cogs:
            commands = self.bot.get_cog(cog.title()).get_commands()
            names = []
            for command in commands:
                names.append(command.name)
            await basics.embed(ctx.channel, ", ".join(sorted(names)))
        elif cog is None:
            entries = ""
            for cog in ctx.bot.cogs:
                names = [
                    command.name for command in self.bot.get_cog(cog).get_commands()
                ]
                if len(names) != 0:
                    entries = entries + f"**{cog}:**\n" f"{', '.join(sorted(names))}\n"
            await basics.embed(ctx.channel, entries)
        else:
            await basics.embed(ctx.channel, "I don't see that cog, boss.")
            return

    @config.command()
    async def init(self, ctx, permit: str = None):
        """Initializes the permissions for the server."""
        basics = self.bot.get_cog("Basics")

        def check(reaction, user):
            return (
                    user == ctx.author
                    and str(reaction.emoji) in reacts
                    and reaction.message.id == sent.id
            )

        sent = await basics.embed(
            ctx.channel, "Are you sure? This will reset my permissions for this Guild."
        )

        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]

        for react in reacts:
            await sent.add_reaction(react)

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", timeout=30, check=check
            )
            await sent.clear_reactions()

            if str(reaction.emoji) == reacts[0]:
                permissions = Permissions(ctx).clear()
                if permit == "permit":
                    permissions.permit()
                elif permit == "restrict":
                    permissions.restrict()
                permissions.init()
                await basics.embed(ctx.channel, "Okay, guild initialized.")

            else:
                await basics.embed(ctx.channel, "Okay, nevermind.")
        except:  # TODO make except more specific
            await sent.clear_reactions()

    @config.command(aliases=["disable"])
    async def enable(self, ctx, cog: str = None, command: str = None):
        basics = self.bot.get_cog("Basics")
        quip = ""
        if cog == "all":
            setting = None
            if ctx.invoked_with == "enable":
                setting = True
                quip = "Okay, all cogs enabled."
            elif ctx.invoked_with == "disable":
                setting = False
                quip = "Okay, all cogs disabled."
            for cog in self.bot.cogs:
                permissions = Permissions(ctx, cog.lower()).load()
                if "enabled" not in permissions.perms:
                    permissions.clear()
                permissions.perms["enabled"] = setting
                permissions.save(write=False)
            Permissions(ctx).write()
        else:
            try:
                if command.lower() == "none":
                    command = None
            except:  # TODO make except more specific
                pass
            permissions = Permissions(ctx, cog, command)
            if not permissions.real():
                await basics.embed(ctx.channel, "... You made that up.")
                return
            permissions.load()
            if "enabled" not in permissions.perms:
                permissions.clear()
            if ctx.invoked_with == "enable":
                permissions.perms["enabled"] = True
                quip = "Okay, enabled."
            elif ctx.invoked_with == "disable":
                permissions.perms["enabled"] = False
                quip = "Okay, disabled."
            permissions.save()
        await basics.embed(ctx.channel, quip)

    @config.command()
    async def clear(self, ctx, cog: str = None, command: str = None):
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        permissions.clear().save()
        await basics.embed(ctx.channel, "Okay, cleared.")

    @config.command(aliases=["disallow", "block", "unblock"])
    async def allow(
            self, ctx, setting: str, added: str = None, cog: str = None, command: str = None
    ):
        basics = self.bot.get_cog("Basics")
        quip = ""
        all = False

        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        permissions.load()

        if setting in ["role", "user", "channel"]:
            if setting == "role":
                if added.lower() == "everyone":
                    added = ctx.guild.default_role
                else:
                    added = await commands.RoleConverter().convert(ctx, added)
            elif setting == "user":
                added = await commands.UserConverter().convert(ctx, added)
            elif setting == "channel":
                if added.lower() == "all":
                    all = True
                else:
                    added = await commands.TextChannelConverter().convert(ctx, added)

            if all:
                if ctx.invoked_with == "allow":
                    permissions.perms["channels_allow_all"] = True
                    quip = "Okay, allowed in all channels."
                elif ctx.invoked_with == "disallow":
                    permissions.perms["channels_allow_all"] = False
                    quip = "Okay, not allowed in all channels."
            elif ctx.invoked_with == "allow":
                permissions.perms[setting + "s_whitelist"].append(added.id)
                quip = f"Okay, {setting} allowed."
            elif ctx.invoked_with == "disallow":
                permissions.perms[setting + "s_whitelist"].remove(added.id)
                quip = f"Okay, {setting} disallowed."
            elif ctx.invoked_with == "block":
                permissions.perms[setting + "s_blacklist"].append(added.id)
                quip = f"Okay, {setting} blocked."
            elif ctx.invoked_with == "unblock":
                permissions.perms[setting + "s_blacklist"].remove(added.id)
                quip = f"Okay, {setting} unblocked."

        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return

        permissions.save()
        await basics.embed(ctx.channel, quip)

    @config.command(aliases=["restrict"])
    async def permit(self, ctx, cog: str = None, command: str = None):
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        quip = ""
        permissions = Permissions(ctx, cog, command).load()
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        if ctx.invoked_with == "permit":
            permissions.permit()
            quip = "Okay, access set to permissive."
        elif ctx.invoked_with == "restrict":
            permissions.restrict()
            quip = "Okay, access set to restrictive."
        permissions.save()
        await basics.embed(ctx.channel, quip)

    @config.command(aliases=["all_channels", "channels"])
    async def channels_allow_all(
            self, ctx, value: str = None, cog: str = None, command: str = None
    ):
        basics = self.bot.get_cog("Basics")
        if cog == "all":
            setting = None
            if value.lower() == "true":
                setting = True
            elif value.lower() == "false":
                setting = False
            elif value.lower() == "none":
                setting = None
            for cog in self.bot.cogs:
                permissions = Permissions(ctx, cog.lower()).load()
                if "channels_allow_all" not in permissions.perms:
                    permissions.clear()
                permissions.perms["channels_allow_all"] = setting
                permissions.save(write=False)
            Permissions(ctx).write()
        else:
            try:
                if command.lower() == "none":
                    command = None
            except:  # TODO make except more specific
                pass
            permissions = Permissions(ctx, cog, command).load()
            if not permissions.real():
                await basics.embed(ctx.channel, "... You made that up.")
                return
            if value.lower() == "true":
                permissions.perms["channels_allow_all"] = True
            elif value.lower() == "false":
                permissions.perms["channels_allow_all"] = False
            elif value.lower() == "none":
                permissions.perms["channels_allow_all"] = None
            else:
                await basics.embed(
                    ctx.channel, "... `true`, `false`, or `none`, please."
                )
                return
            permissions.save()
        await basics.embed(ctx.channel, "Okay, setting saved.")

    @config.command()
    async def perms(self, ctx, cog: str = None, command: str = None):
        # TODO: Prettify and paginate.
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass

        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        permissions.load()
        if command is not None:
            query = f"{cog}/{command}"
        else:
            query = cog

        title = f"Permissions for {query}:"
        body = f"```json\n{json.dumps(permissions.perms, indent=4, sort_keys=True)}```"
        await basics.custom_embed(
            ctx, text=f"title='{title}' delete='false' description='{body}'"
        )

    @config.command()
    async def real(self, ctx, cog: str = None, command: str = None):
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        if permissions.real():
            quip = "That one's real."
        else:
            quip = "... You made that up."
        await basics.embed(ctx, quip)

    @config.command()
    async def set_emote(self, ctx, name, emote):
        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        emotes.set(name, emote)
        await basics.embed(ctx.channel, f"{name.title()} was set to {emote}.")

    @config.command()
    async def reset_emote(self, ctx, name):
        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        emotes.reset(name)
        await basics.embed(ctx.channel, f"{name.title()} was reset to {emotes.defaults[name]}.")

    @commands.group(aliases=['settings'])
    @commands.has_guild_permissions(manage_guild=True)
    @commands.guild_only()
    async def guild_settings(self, ctx: commands.context, setting: str, *args):
        msgs = messages.Messages()
        if setting in settings.settings_list:
            if setting != 'get' and setting != 'reset':
                await msgs.send_message(ctx.channel, await settings.settings_list[setting].set_value(ctx, *args))
        elif setting == 'get' and len(args) > 0:
            if args[0] == 'all':
                await msgs.send_message(ctx.channel, str(await settings.get_all_settings(str(ctx.guild.id))))
            elif args[0] in settings.settings_list:
                await msgs.send_message(ctx.channel,
                                        str(await settings.settings_list[args[0]].get_value(str(ctx.guild.id))))
        elif setting == 'reset' and len(args) > 0:
            if args[0] == 'all':
                await settings.reset_all_settings(str(ctx.guild.id))
                await msgs.send_message(ctx.channel, 'reset all settings')
            elif args[0] in settings.settings_list:
                await settings.settings_list[args[0]].reset_value(str(ctx.guild.id))
                await msgs.send_message(ctx.channel, 'setting reset')

    @commands.Cog.listener("on_ready")
    async def setup(self):

        print("Logged in as " + str(self.bot.user))
        await data.check_tables(self.bot)
        async with data.Connection() as conn:
            try:
                await self.bot.change_presence(
                    activity=discord.Game(
                        name=await data.r.table("config")
                            .get("now_playing")["value"]
                            .run(conn)
                    )
                )
                print("now_playing set.")
            except:  # TODO make except more specific
                print("now_playing field not found.")


def setup(bot):
    bot.add_cog(Configure(bot))
