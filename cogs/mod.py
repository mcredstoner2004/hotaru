from helpers.objects import Emotes
from discord.ext import commands
from helpers.helpers import get_settings
import discord
import inflect


def is_confident():
    def predicate(ctx):
        confidence = get_settings("confidence")
        confidants = get_settings("confidants")
        return ctx.author.id in confidants and ctx.channel.id in confidence

    return commands.check(predicate)


class Mod(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    @is_confident()
    async def purge(self, ctx, qty: int, sure: str = None, user: discord.User = None):
        basics = self.bot.get_cog("Basics")
        p = inflect.engine()

        def check(reaction, user):
            return str(reaction.emoji) in reacts and user.id == ctx.author.id

        def user_check(message):
            return message.author.id == user.id

        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]

        affirms = ["yes", "confirm", "sure"]

        if sure is None:
            sent = await basics.embed(
                ctx.channel,
                f"Purge {qty} {p.plural('message', qty)}, you sure about that?",
            )
            for react in reacts:
                await sent.add_reaction(react)

            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", timeout=30, check=check
                )
                if str(reaction.emoji) == reacts[0]:
                    await ctx.channel.purge(limit=qty, before=ctx.message)
            except:  # TODO make except more specific
                pass

            await sent.delete()

        elif sure in affirms:
            if user != None:
                purge_check = user_check
            else:
                purge_check = None
            await ctx.channel.purge(limit=qty, before=ctx.message, check=purge_check)

        await ctx.message.delete()

    @commands.Cog.listener("on_message")
    async def confident_logger(self, message):
        confidence = get_settings("confidence")

        if message.channel.id in confidence:
            entry = (
                f"[{message.created_at.ctime()}] {message.author}: {message.content}"
            )

            if len(message.attachments) > 0:
                for attachment in message.attachments:
                    entry = entry + f"\n{attachment.filename}: {attachment.url}"

            with open("data/confidence.log", "a+", encoding="utf-8") as log:
                log.seek(0)
                data = log.read(100)
                if len(data) > 0:
                    log.write("\n")
                log.write(entry)

    @commands.Cog.listener("on_message_edit")
    async def confident_edit(self, before, after):
        confidence = get_settings("confidence")

        if after.channel.id in confidence:
            entry = f"[{after.created_at.ctime()}] {after.author}:* {after.content}"

            if len(after.attachments) > 0:
                for attachment in after.attachments:
                    entry = entry + f"\n{attachment.filename}: {attachment.url}"

            with open("data/confidence.log", "a+", encoding="utf-8") as log:
                log.seek(0)
                data = log.read(100)
                if len(data) > 0:
                    log.write("\n")
                log.write(entry)

    @commands.Cog.listener("on_raw_reaction_add")
    async def prune(self, event):
        confidence = get_settings("confidence")
        confidants = get_settings("confidants")

        if (
            event.channel_id in confidence
            and event.user_id in confidants
            and str(event.emoji) == "🗑️"
        ):
            channel = self.bot.get_channel(event.channel_id)
            message = await channel.fetch_message(event.message_id)
            await message.delete()


def setup(bot):
    bot.add_cog(Mod(bot))
