from discord.ext import commands
from helpers.checks import allowed_channel, server_manager_or_perms
import discord
import json
import os
import re
from hotaru import data
from helpers import settings


class Basics(commands.Cog):
    def __init__(self, bot):
        settings.FloatSetting("delete_after", default_value=30, min_val=0, lopen=False,
                              max_val=None, success_msg="alright, messages will now be deleted after {value} seconds",
                              range_err_msg="I can't delete messages before I send them, you know?")

        self.bot = bot

    def get_token(self):
        # Check for a token.json file that has Hotaru's login token.

        try:
            with open("token.json", "r") as file:
                token = json.load(file)

            return token

        except Exception as e:
            print(e)
            print("No token.json present")
            exit()

    def parse_command(self, text_input, trigger_check=""):
        # This separates the trigger and / or command from the argument.

        # This will separate the trigger, if it exists.
        if text_input.startswith(trigger_check):
            trigger_length = len(trigger_check)
            text_input = text_input[trigger_length:]

        # This will check for a space-delineated argument and separate it
        # from the command.
        if " " in text_input:
            command_index = text_input.find(" ")
            command = text_input[:command_index].lower()
            argument = text_input[command_index + 1:]

        else:
            command = text_input.lower()
            argument = ""

        return command, argument

    async def embed(self, channel, phrase, description: str = None, delete_after=None):
        # Hotaru will speak as an embed to a given channel.
        embed = discord.Embed(description=phrase, color=0x00FF00)
        message = await channel.send(description, embed=embed, delete_after=delete_after)
        return message

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def custom_embed(self, ctx, *, text):
        embed_parts = [
            x.strip("'\"=")
            for x in re.split(r"( |\"[^\"]*?\"|'[^\']*?')", text)
            if x.strip()
        ]
        properties = []
        if len(embed_parts) % 2 == 0:
            index = int(len(embed_parts) / 2)
            for x in range(index):
                properties.append([embed_parts[x * 2], embed_parts[(x * 2) + 1]])
        embed = discord.Embed(color=0x00FF00)
        phrase = None
        channel = None
        delete = True
        message = None
        for property in properties:
            if property[0] in ["image", "set_image"]:
                embed.set_image(url=property[1])
            elif property[0] in ["thumbnail", "set_thumbnail"]:
                embed.set_thumbnail(url=property[1])
            elif property[0] in ["say", "content"]:
                phrase = property[1]
            elif property[0] == "channel":
                channel = await commands.TextChannelConverter().convert(
                    ctx, property[1]
                )
            elif property[0] == "delete" and property[1].lower() == "false":
                delete = False
            elif property[0] in ["footer", "set_footer"]:
                embed.set_footer(text=property[1])
            elif property[0] in ["author", "set_author"]:
                embed.set_author(name=property[1])
            elif property[0] in ["msg", "message"]:
                message = await commands.MessageConverter().convert(ctx, property[1])
            elif property[0] in ["color", "colour"]:
                embed.color = int(property[1])
            else:
                setattr(embed, property[0], property[1])
        if delete:
            await ctx.message.delete()
        if message is not None:
            await message.edit(content=phrase, embed=embed)
        elif channel is None:
            await ctx.send(content=phrase, embed=embed)
        else:
            await channel.send(content=phrase, embed=embed)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def say(self, ctx, *, phrase: str):
        # Hotaru will check if the first word is a channel.  She'll
        # post the message to the given channel if one exists,
        # otherwise she'll post it to the context channel.  She'll
        # delete the invoking message, as well.
        if phrase.startswith("<#"):
            channel_id = int(phrase.partition("<#")[2].partition(">")[0])
            channel = self.bot.get_channel(channel_id)
            phrase = phrase.partition("> ")[2]
        else:
            channel = ctx.channel

        await ctx.message.delete()
        await self.embed(channel, phrase)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def playing(self, ctx, *, phrase: str):
        # Hotaru will update her playing status, and then save the new
        # status to her settings so she'll remember it for her next
        # boot.
        confirm = f"Okay, I guess I'm playing {phrase}, now."
        await self.bot.change_presence(activity=discord.Game(name=phrase))
        await self.embed(ctx.channel, confirm)
        async with data.Connection() as conn:
            await data.r.table("config").insert(
                {"id": "now_playing", "value": phrase}, conflict="replace"
            ).run(conn)
        print("now_playing updated")

    @playing.error
    async def playing_error(self, ctx, e):
        # TODO: Capture error and differentiate bad arg from failed check.
        print(e)
        await self.embed(ctx.channel, "Weeb.")

    @commands.command(aliases=["hi"])
    @commands.guild_only()
    @server_manager_or_perms()
    async def hello(self, ctx):
        # She says a special greeting for nice people.
        await self.embed(ctx.channel, "Hey, staying safe?")

    @hello.error
    async def hello_error(self, ctx, e):
        print(e)
        await self.embed(ctx.channel, "What do you want?")

    @commands.command(aliases=["coffee?", "exit"])
    @commands.guild_only()
    @server_manager_or_perms()
    async def coffee(self, ctx):
        # Hotaru takes a coffee break.
        await self.embed(ctx.channel, "Hunh? Yeah, I could go for a cup, see ya.")
        await self.bot.close()
        print("Client closed.")
        try:
            exit()
        except SystemExit as e:
            print(e)
            os._exit(1)

    @coffee.error
    async def coffee_error(self, ctx, e):
        print(e)
        await self.embed(ctx.channel, "Excuse me, 2 meters? Thanks.")

    @commands.command()
    @commands.guild_only()
    async def list(self, ctx):
        # Hotaru lists the known help commands
        basics = self.bot.get_cog("Basics")
        async with data.Connection(str(ctx.guild.id)) as conn:
            cursor = await data.r.table("help_info").run(conn)
            items = []
            async for doc in cursor:
                items.append(doc["id"])
            if len(items) > 0:
                items.sort()
                string = ", ".join(items)
                await basics.embed(
                    ctx.channel, f"Yeah, you can ask me about any of these:\n{string}"
                )
            else:
                await basics.embed(
                    ctx.channel, "Hunh... empty list. That's someone else's problem, haha."
                )

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def permissions_check(self, ctx):
        await self.embed(ctx.channel, "Hunh, it worked.")


def setup(bot):
    bot.add_cog(Basics(bot))
