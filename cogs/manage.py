"""Bot management functions for the bot owner."""


from discord.ext import commands
import os
import subprocess


class Manage(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(aliases=["git"])
    @commands.guild_only()
    @commands.is_owner()
    async def manage(self, ctx):
        """The main manage group. git is an alias purely for typing git pull."""
        basics = self.bot.get_cog("Basics")
        if ctx.invoked_subcommand is None:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")

    @manage.command()
    async def pull(self, ctx):
        """Performs git pull in the terminal."""
        basics = self.bot.get_cog("Basics")
        try:
            if ".git" in os.listdir():
                subprocess.call("git pull", shell=True)
            await basics.embed(ctx.channel, "Updates, uh, updated.")
        except:  # TODO make except more specific
            await basics.embed(ctx.channel, "This ain't it, chief.")

    @manage.command()
    async def reload(self, ctx, cog: str = None):
        """Reloads a cog after pulling updates.

        Args:
            cog (str, optional): The cog to reload. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        if cog is not None:
            try:
                self.bot.reload_extension("cogs." + cog)
                await basics.embed(ctx.channel, f"Alright, the {cog} cog was reloaded.")
            except Exception as e:
                print(e)
                await basics.embed(ctx.channel, f"Something went wrong...")

    @manage.command()
    async def load(self, ctx, cog: str = None):
        """Load a new cog.

        Args:
            cog (str, optional): The cog to load. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        if cog is not None:
            try:
                self.bot.load_extension("cogs." + cog)
                await basics.embed(ctx.channel, f"Alright, the {cog} cog was loaded.")
            except Exception as e:
                print(e)
                await basics.embed(ctx.channel, f"Something went wrong...")

    @manage.command()
    async def unload(self, ctx, cog: str = None):
        """Unload an existing cog.

        Args:
            cog (str, optional): The cog to unload. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        if cog is not None:
            try:
                self.bot.unload_extension("cogs." + cog)
                await basics.embed(ctx.channel, f"Alright, the {cog} cog was unloaded.")
            except Exception as e:
                print(e)
                await basics.embed(ctx.channel, f"Something went wrong...")


def setup(bot):
    bot.add_cog(Manage(bot))
