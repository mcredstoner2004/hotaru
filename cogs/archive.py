"""The archive cog deals with modifications to the faq, user, and ignore lists."""


from discord.ext import commands
from helpers.checks import allowed_channel, server_manager_or_perms
from hotaru import data


class Archive(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def sorry(self, ctx):
        basics = self.bot.get_cog("Basics")
        if await self.bot.is_owner(ctx.author):
            await basics.embed(ctx.channel, "S-sorry, I'm not sure...")
        else:
            await basics.embed(ctx.channel, "\\*sigh...\\*")

    def group_parser(self, ctx):
        """Parses the group used to invoke."""
        return ctx.message.content.lstrip(self.bot.command_prefix).partition(" ")[0]

    def cases(self, command):
        """A switch-case dependant on which command was used to invoke."""
        cases = {"faq": "help_info", "user": "user_info", "ignore": "ignore_list"}
        return cases.get(command)

    @commands.group(aliases=["user", "ignore"])
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def faq(self, ctx):
        """The main group for the dictionary commands.

        Depending on which name is used to invoke, a different dictionary will
        be referenced for the subcommand.
        """
        if ctx.invoked_subcommand is None:
            await self.sorry(ctx)

    @faq.command()
    async def add(self, ctx, word: str = None, *, phrase: str = None):
        """Add a phrase to a dictionary.

        Args:
            word (str, optional): The word to add. Defaults to None.
            phrase (str, optional): The description to add. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        command = self.group_parser(ctx)
        case = self.cases(command)
        if word is None:
            await basics.embed(ctx.channel, "You have to tell me what to add, y'know?")

        else:
            if phrase is None:
                await basics.embed(ctx.channel, f"Okay, but what about {word}?")
            else:
                async with data.Connection(str(ctx.guild.id)) as conn:
                    await data.r.table(case).insert(
                        {
                            "id": word,
                            "description": phrase,
                        },
                        conflict="replace",
                    ).run(conn)
                await basics.embed(
                    ctx.channel,
                    f"Okay, {word} was added to the {command} dictionary, woo.",
                )

    @faq.command()
    async def remove(self, ctx, word: str = None):
        """Strikes a phrase from a dictionary.

        Args:
            word (str, optional): The word to remove. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        command = self.group_parser(ctx)
        case = self.cases(command)
        if word is None:
            await basics.embed(ctx.channel, "Uh, remove what?")
        else:
            print()
            async with data.Connection(str(ctx.guild.id)) as conn:
                await data.r.table(case).get(word).delete().run(conn)
            await basics.embed(
                ctx.channel,
                f"Okay, {word} was stricken from the {command} dictionary, aww.",
            )

    @faq.command()
    async def list(self, ctx):
        """List the keys from a dictionary."""
        basics = self.bot.get_cog("Basics")

        command = self.group_parser(ctx)
        case = self.cases(command)
        async with data.Connection(str(ctx.guild.id)) as conn:
            cursor = await data.r.table(case).run(conn)

            items = []
            async for doc in cursor:
                items.append(doc["id"])
            if len(items) > 0:
                items.sort()
                string = ", ".join(items)
                await basics.embed(ctx.channel, f"Here's what I know:\n{string}")
            else:
                await basics.embed(ctx.channel, "The list is empty, boss.")


def setup(bot):
    bot.add_cog(Archive(bot))
