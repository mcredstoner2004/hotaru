"""The Utility cog covers miscellaneous functionality not covered by the other cogs."""


import base64
from discord.ext import commands
import re
from helpers.mew_helpers import *


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(aliases=["base64"])
    async def b64decode(self, ctx: commands.context, b64: str):
        """Decodes a b64 string
        :rtype: str
        :param ctx: the context in which the command was invoked
        :param b64: the b64 string to decode
        """
        if (Author := ctx.author).dm_channel is None:
            # creates a DMChannel with the user that sent the command if it doesn't exist
            await Author.create_dm()
        async for dm_message in Author.dm_channel.history():
            if dm_message.author == self.bot.user:
                await dm_message.delete()  # deletes past messages sent to the user
        decoded_str = base64.b64decode(b64).decode("ascii")
        t, total_links = remove_links(decoded_str)
        if total_links > 0:
            link = re.sub(r"\.", "%2E", decoded_str.partition("://")[2])
            # if the b64 string sent contains a link, obfuscate it
            await Author.dm_channel.send(link, delete_after=30)
        else:
            # send it as is otherwise
            await Author.dm_channel.send(decoded_str, delete_after=30)


def setup(bot):
    bot.add_cog(Utility(bot))
