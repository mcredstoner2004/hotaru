"""The error cog is called when a command isn't recognized.

It will check the help and user dictionaries to see if it should respond.
"""

from discord.ext import commands
from hotaru import data


class Error(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def check_faq(self, ctx, phrase):
        basics = self.bot.get_cog("Basics")
        phrase = phrase.lower()
        async with data.Connection(str(ctx.guild.id)) as conn:
            description = await data.r.table("help_info").get(phrase).run(conn)
        if description is not None:
            await basics.embed(ctx.channel, description["description"])
        else:
            await self.say_user(ctx, phrase)

    async def say_user(self, ctx, phrase):
        basics = self.bot.get_cog("Basics")
        key_matches = []
        async with data.Connection(str(ctx.guild.id)) as conn:
            cursor = await data.r.table("user_info").run(conn)
            async for record in cursor:
                # This list is populated with all matching keys.
                key = record["id"]
                if phrase.startswith(key):
                    key_matches.append(key)

            if len(key_matches) != 0:
                longest_match = ""
                longest_length = 0

                # Afterwards, only the shortest match is presented.
                for match in key_matches:

                    if len(match) > longest_length:
                        longest_length = len(match)
                        longest_match = match

                await basics.embed(
                    ctx.channel,
                    await data.r.table("user_info")
                        .get(longest_match)["description"]
                        .run(conn),
                )

    @commands.Cog.listener("on_command_error")
    async def dict_checker(self, ctx, e):
        """The listener that calls the above dictionary checks."""
        text = ctx.message.content.lstrip(self.bot.command_prefix).partition(" ")[0]
        print(e)
        await self.check_faq(ctx, text)


def setup(bot):
    bot.add_cog(Error(bot))
